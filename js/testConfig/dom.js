import { JSDOM } from 'jsdom';
import localStorage from 'localStorage';

const dom = new JSDOM('<!doctype html><html><head><script/></head><body><div id="appLogin"><input id="login"/></div></body></html>');
const { window } = dom;
const { navigator } = dom;

function copyProps(src, target) {
    const props = Object.getOwnPropertyNames(src)
        .filter(prop => typeof target[prop] === 'undefined')
        .reduce((result, prop) => ({
            ...result,
            [prop]: Object.getOwnPropertyDescriptor(src, prop),
        }), {});
    Object.defineProperties(target, props);
}

global.dom = dom;
global.window = window;
global.navigator = navigator;
global.document = window.document;
global.localStorage = localStorage;
global.window.matchMedia =
    window.matchMedia ||
    (() => {
        return { matches: false, addListener: () => {}, removeListener: () => {} };
    });
global.navigator = {
    userAgent: 'node.js',
    javaEnabled: () => true
};

copyProps(window, global);
