import mochaSnapshots from 'mocha-snapshots';
import sinon from 'sinon';
import { expect, assert, should } from 'chai';
// import { mount, render, shallow, configure } from 'enzyme';
// import Adapter from 'enzyme-adapter-react-16';
// import React from "react";

// let Datejs = require('datejs');

// configure({ adapter: new Adapter() });

// global.mochaSnapshots = mochaSnapshots;
// mochaSnapshots.setup({ sanitizeClassNames: false });

global.expect = expect;
global.assert = assert;
global.should = should();

global.sinon = sinon;

// global.mount = mount;
// global.render = render;
// global.shallow = shallow;
// global.React = React;

// old global variable
global.StocksInfo = null;

// global.itRenders = Component => {
//     const wrapper = shallow(Component);
//     expect(shallow(Component).length).to.eql(1);
//     return wrapper;
// };
