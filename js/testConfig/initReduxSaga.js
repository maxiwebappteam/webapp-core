import { applyMiddleware, createStore } from 'redux';
import { configCore } from '../mockData/configCoreMock';
import rootSaga from '../roots/rootSaga';
import sessionData from '../mockData/mockSessionData';
import rootReducer from '../roots/rootReducer';
import createSagaMiddleware from 'redux-saga';

const initialState = {
    sessionData: sessionData,
    configCore: configCore,
    userLocalSettings: {
        ...configCore.modules.userLocalSettings,
        isLocalStorageSupport: true
    },
    strings: {},
    coreModel: {
        loadedRates: {},
        userSettings: {
            accountIsActive: null,
            accountNames: [],
            accounts: [],
            closeDealMinInterval: null,
            currentAccountName: null,
            equityAlertLevel1: null,
            equityAlertLevel2: null,
            equityAlertLevel3: null,
            limitPriceMaxPercent: null,
            limitPriceMinPercent: null,
            securitySettings: {},
            stocksInfo: {},
            stopLossMaxPercent: null,
            stopLossMinPercent: null,
            stopPriceMaxPercent: null,
            stopPriceMinPercent: null,
            takeProfitMaxPercent: null,
            takeProfitMinPercent: null,
            tradingSessions: {}
        },
        userStatistic: {
            equityAlert: {
                minDepositLow: null,
                minDepositHigh: null,
                minDepositMid: null,
                level: null,
            },
            unrealizedPnlValue: null,
            freeBalanceValue: null,
            totalEquityValue: null,
            marginUsedValue: null,
            currencySymbol: null,
            alertBalance: null,
            balanceValue: null,
        },
        accountSettings: {
            currencySymbol: null,
            currency: null,
            ratesCount: {
                commodity: null,
                indexes: null,
                stocks: null,
                currency: null,
                cryptoCurrency: null
            },
            equityAlertLevel: {
                lev1: null,
                lev2: null,
                lev3: null
            }
        },
        serverTime: null,
        device: {},
    },
    modal: {
        dialog: {
            isShow: true,
            context: 'settings'
        },
        error: {
            isShow: true,
            context: {
                isTechInfo: true,
                errorCode: 22,
                response: 'Response',
                time: 'MM/DD/YY 00:00:00',
            }
        }
    }
};

const sagaMiddleware = createSagaMiddleware();

global.store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);
