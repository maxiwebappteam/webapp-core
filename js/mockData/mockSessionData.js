export default {
    isConnectAlive: true,
    lang: 'ru',
    sid: '4b2f1d34-5bbb-4209-88fe-348f4bf4b77f',
    sids: {
        '4b2f1d34-5bbb-4209-9999-348f4bf4b77f': 1530876414505,
    },
    sp: 'v2UpVgFqMjX0CC7NyOi5whPaWQYuNQkfZFSuWNd8eburGm9b7Iirw2zqiQ5RAE8ADumxqaEfAdg4x6zotojMpkY7aLdqtsEQ5uyW3F5i4BJcFXdZn8VkD7OynU3yqrzSoUEQfijgwD7e90GVHIldi5X65Usc9gfn6l4vm4mPwU8oWMVXjrOdVuQIkBZsK7GcRAi6q5r2aHoompRNE3POsw9FA1ZlDKsNiEmoeHaugyR9bn4dCsw2DDvEaClaUCbs',
    su: 'Lquz4SsIAg7XZUdMLGEnCDMLOioeU83F0gyOIok2vBzBMss7Uz12zBRJpqjKpmYrff0sRujlmWqMhviElBxJEDznqPqCrGzXINJopEwsGXRXpa026upkt4s3fR6exsgh',
    th: 'dark',
    tid: '1530862099088',
    uid: 'sbdev1@i.ua',
}