import { configSite } from './configMock';

const currentPlatform = 'crypto';
const defaultTheme = 'darkTheme';

export const configCore = {
    platforms: ['crypto'],
    currentPlatform: currentPlatform,
    customMarker: `${currentPlatform} ${configSite.version}`,
    loginPrefix: configSite.loginPrefix,
    title: configSite.title,
    favIcon: configSite.favIcon,
    bigLogoSrc: configSite.bigLogoSrc,
    loginservice: configSite.loginservice,
    keepaliveservice: configSite.keepaliveservice,
    sockjspath: configSite.sockjspath,
    smallLogoHeader: configSite.smallLogoHeader,
    accountpage: configSite.accountpage,
    depositPage: configSite.depositPage,
    autochartistpage: configSite.autochartistpage,
    chartstpage: configSite.chartstpage,
    tradingCentralPage: configSite.tradingCentralPage,
    homePage: configSite.homePage,
    loginLang: configSite.loginLang,
    sitelangs: configSite.sitelangs,
    isOnlineChat: configSite.isOnlineChat,
    jivoWidgetId: configSite.jivoWidgetId,
    isEnChatDisabled: configSite.isEnChatDisabled,
    mobileVersionURL: configSite.mobileVersionURL,
    desktopVersionURL: configSite.desktopVersionURL,
    externalLinks: configSite.externalLinks,
    sharesLinks: configSite.sharesLinks,
    helpLinks: configSite.helpLinks,
    helpLinksText: configSite.helpLinksText,
    isKeepAlive: configSite.isKeepAlive,
    keepaliveTimeOut: configSite.keepaliveTimeOut,
    validateEMail: configSite.validateEMail,
    timerInterval: 1000,
    economicEventsLink: configSite.economicEventsLink,
    themes: configSite.themes,
    features: {
        demoTrendLine: configSite.features.demoTrendLine,
        economicEvents: configSite.features.economicEvents
    },
    modules: {
        chart: {
            isActive: true,
            pair: { ID: 23 },
            period: '1',
            chartType: 'candle',
            typeData: 'mid',
            state: false,
            tabsQuantity: 16,
            connectorOptions: {
                url: '',
                wsUrl: '',
                sockjspath: '',
                authData: null,
                source: 'integration',
                type: 'marketsNoConn'
            },
            wsUrl: '',
            url: '',
            settingsUrl: '',
            lang: 'market_lang',
            isHeaderHidden: false,
            isSidebarHidden: false,
            isFullScreen: false,
            isDataPanel: false,
            isAutoRestore: true,
            rowsid: [],
            defaultId: 23,
            typeThemes: 'default',
            isConfigpage: false,
            newsSourceUrl: '',
            eventsSourceUrl: '',
            quotationCountriesUrl: '',
            isShowPriceLineOfLastBar: true,
            candleWidth: 26,
            isPipsShow: false,
            modules: {
                headerButtons: {
                    searchQuotationButton: false,
                    createTabButton: true,
                    fullScreenButton: true,
                    chartTypeButton: true,
                    periodsButton: true,
                    indicatorButton: true,
                    splitterButton: false,
                    createTypeDataButton: false,
                    typeDataButton: false,
                    htmlTypeDataButton: true,
                    createPipsButton: true,
                    settingsButton: true,
                    createFavouritesButton: true,
                },
            }
        },
        instrumentsList: {
            filterInstrumentsList: ['Favorites', 'All', 'Popular', 'Commodity', 'CryptoCurrency', 'Currency', 'Index', 'Stock'],
            defaultCategory: 'All',
            isActive: true,
        },
        sidebar: {
            isActive: true,
            sidebarElements: {
                option: {
                    id: 'sidebarOptionType',
                    blockNameKey: 'OptionType',
                    textButton: 'optionType',
                    imgButton: null,
                    imgButtonHover: null,
                    classNameKey: 'type',
                    dataAt: 'at-sidebar-optionlnk',
                    classNameEdge: 'btn-type__image-area btn-type--active hidden-element.btn-type__image-area btn-type hidden-element',
                    onOpen: null,
                },
                charts: {
                    id: 'sidebarCharts',
                    blockNameKey: 'Charts',
                    textButton: 'chartsBinary',
                    imgButton: null,
                    imgButtonHover: null,
                    classNameKey: 'charts',
                    dataAt: 'at-trading-chartslnk',
                    classNameEdge: 'btn-charts__image-area btn-charts--active hidden-element.btn-charts__image-area btn-charts hidden-element',
                    onOpen: null,
                },
                deals: {
                    id: 'sidebarDeals',
                    blockNameKey: 'Deals',
                    textButton: 'positions',
                    imgButton: null,
                    imgButtonHover: null,
                    classNameKey: 'deals',
                    dataAt: 'at-trading-dealslnk',
                    classNameEdge: 'btn-deals__image-area btn-deals--active hidden-element.btn-deals__image-area btn-deals hidden-element',
                    onOpen: null,
                },
                externalLinks: {
                    id: 'linksExternal',
                    btnId: 'linksExternalInner',
                    className: 'tab-external-links',
                    iconClass: 'tools-image-area',
                    textButton: 'links',
                    dataAt: 'at-trading-externallnk',
                    menuSettings: null,
                    isShow: !!(configSite.externalLinks && Object.keys(configSite.externalLinks).length),
                },
                sharesLinks: {
                    id: 'linksShares',
                    btnId: 'linksSharesInner',
                    className: 'tab-shares-links',
                    iconClass: 'shares-image-area',
                    textButton: 'sharesLinks',
                    dataAt: 'at-trading-shareslnk',
                    menuSettings: null,
                    isShow: !!(configSite.sharesLinks && Object.keys(configSite.sharesLinks).length),
                },
                helpLinks: {
                    id: 'linksHelp',
                    btnId: 'linksHelpInner',
                    className: 'tab-help-links',
                    iconClass: 'help-image-area',
                    textButton: 'helpLinks',
                    dataAt: 'at-trading-helplnk',
                    menuSettings: null,
                    isShow: !!(configSite.helpLinksText && Object.keys(configSite.helpLinksText).length),
                },
            },
        },
        header: {
            isActive: true,
            headerButtons: {
                settings: {
                    id: 1,
                    name: 'settings',
                    isShow: true,
                    className: 'buttons-wrapper__settings-btn',
                    dataAttribute: 'at-settingsbtn',
                    dataRelation: 'tooltip',
                },
                fullScreen: {
                    id: 2,
                    name: 'fullScreen',
                    isShow: true,
                    className: 'buttons-wrapper__full-screen-btn',
                    dataAttribute: 'at-fullscrbtn',
                    dataRelation: 'tooltip',
                },
                account: {
                    id: 3,
                    name: 'account',
                    isShow: true,
                    className: 'buttons-wrapper__profile-btn',
                    dataAttribute: 'at-myaccbtn',
                    dataRelation: 'tooltip',
                },
                logout: {
                    id: 4,
                    name: 'logout',
                    isShow: true,
                    className: 'buttons-wrapper__logout-btn',
                    dataAttribute: 'at-logoutbtn',
                    dataRelation: 'tooltip',
                }
            }
        },
        jivoChat: {
            isActive: true,
        },
        tooltips: {
            isActive: true,
        },
        historyTable: {
            isActive: true,
        },
        serverUserLocalSettingsKeys: {
            blocksActive: ['Charts', 'Deals', 'OptionType'],
            currentTheme: 'value'
        },
        userLocalSettings: {
            isActive: true,
            html5NotifPermission: false,
            isFullScreenPlatform: false,
            accountSettings: {
                blocksActive: {
                    Charts: true,
                    Instruments: true,
                    Deals: true,
                    OptionType: true,
                },
                blocksLayout: {},
                instrumentFilter: {},
                settings: {
                    oneClickTrading: true,
                    linksIsModal: true,
                    isMute: false,
                },
                confirmationSettings: {
                    oneClickTrading: false,
                },
                currentTheme: defaultTheme,
                historyTable: {
                  activeTab: 'tab-1',
                },
            },
        },
        hintPanel: {
            isActive: true
        }
    },
    strings: {
        resources: null,
        dialogresources: null,
        errorCodes: null,
        locChart: null
    },
    managers: {
        modal: {
            isLoad: true,
            isCloseOnEsc: true,
            isCloseOnOverlayClick: true,
        },
        strings: {
            isLoad: true,
        },
        localStorage: {
            isLoad: true,
        },
        resizeArea: {
            isLoad: true,
        },
        userLocalSettings: {
            isLoad: true,
        },
        windowData: {
            isLoad: true,
        },
    }
};

