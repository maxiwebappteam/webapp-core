export const getSockPath = state => state.configCore.sockjspath;
export const getSockUser = state => state.sessionData.su;
export const getLanguage = state => state.sessionData.lang;
export const getSessionTimeId = state => state.sessionData.tid;
export const getSockPassword = state => state.sessionData.sp;
export const getSessionId = state => state.sessionData.sid;
export const getSessionIds = state => state.sessionData.sids;
export const getSessionData = state => state.sessionData;
export const isConnectionAlive = state => state.sessionData.isConnectAlive;
export const getCRMApiUrl = state => state.configCore.crmApiUrl;
export const getCustomMarker = state => state.configCore.customMarker;
export const isLocalStorageSupport = state => state.userLocalSettings.isLocalStorageSupport;
export const getKeepAliveParams = state => ({
    keepAliveService: state.configCore.keepaliveservice,
    keepAliveTimeOut: state.configCore.keepaliveTimeOut,
    isKeepAlive: state.configCore.isKeepAlive,
});
export const getReconnectParams = state => ({
    loginService: state.configCore.loginservice,
    keepAliveTimeOut: state.configCore.keepaliveTimeOut,
});

// ToDo added mock selectors for CORE_MODEL
export const getLoadedRates = state => state.coreModel.loadedRates;
export const getUserSettings = state => state.coreModel.userSettings;
export const getUserStatistic = state => state.coreModel.userStatistic;
export const getCurrentSymbol = state => state.coreModel.accountSettings.currencySymbol;
export const getEquityAlertLevels = state => state.coreModel.accountSettings.equityAlertLevel;

