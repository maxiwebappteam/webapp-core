export const getMockRequestKeys = () => (['logout', 'get_trading_status', 'get_stocks_info', 'get_autochartist_signals', 'get_autochartist_chart_patterns', 'get_autochartist_fibonacci',
    'get_autochartist_key_levels', 'get_time', 'get_user_settings', 'get_user_account', 'get_user_statistics', 'get_user_rates', 'add_favorites', 'del_favorites',
    'get_favorites', 'get_open_deals', 'get_user_trading_signals', 'set_trading_signal_shown', 'hide_trading_signal', 'send_open_market_order', 'duplicate_open_deal',
    'change_open_deal', 'send_close_market_order', 'get_closed_deals', 'get_limit_stop_orders', 'send_order', 'change_order', 'cancel_order_bracket', 'cancel_order',
    'change_account', 'get_chart_data', 'get_trend_line', 'get_closed_binary_options', 'get_open_binary_options', 'send_buy_binary_option_order', 'send_sell_binary_option_order',
    'duplicate_open_binary_option', 'subscribe_options', 'set_user_local_settings', 'get_user_local_settings', 'subscribe_option_buy_out', 'unsubscribe_option_buy_out',
]);

export const getMockCoreRequestKeys = () => (['logout', 'get_trading_status']);
export const getCoreRequestSender = () => getMockCoreRequestKeys().reduce((acc, key) => ({
    ...acc,
    [key.toUpperCase()]: () => ({ type: key, status: true })
}), {});

export const getParams = () => ({
    symbol: 'symbol',
    currency: 'currency',
    id: 'id',
    active: 'active',
    direction: 'direction',
    amount: 1000000,
    dealId: 'dealId',
    stopPrice: 2000000,
    stopAmount: 3000000,
    takePrice: 4000000,
    takeAmount: 5000000,
    range: 'range',
    fromdate: 150000000000,
    todate: 160000000000,
    ordertype: 'ordertype',
    price: 6000000,
    orderId: 'orderId',
    accountName: 'accountName',
    chartsArr: [["AUDCAD", "MINUTE"], ["Bitcoin", "WEEK"]],
    securities: ["AUDCAD", "Bitcoin"],
    timezone: 'timezone',
    signalId: 'signalId',
    binaryOptionId: 'binaryOptionId',
    putCall: "PUT",
    executionPrice: 7000000,
    binaryOptionExpirationDate: 170000000000,
    binaryOptionStrikePrice: 8000000,
    binaryOptionType: "SHORT_TERM",
    areaMarker: 111
});