export const getMockKesyUpperLevel = () => (['update', 'response', 'event']);
export const getMockKesyUpdateLevel = () => (['get_user_rates', 'get_user_statistics', 'get_time',
    'get_trading_status', 'get_user_trading_signals', 'get_user_trading_signals', 'subscribe_options',
    'subscribe_option_buy_out', 'crm_single_sign_on']);
export const getMockKesyResponseLevel = () => ([
    'logout', 'get_favorites', 'get_trading_status', 'get_user_settings', 'get_user_account',
    'get_user_statistics', 'get_user_rates', 'get_time', 'get_chart_data', 'get_trend_line', 'get_open_deals',
    'send_open_market_order', 'change_open_deal', 'duplicate_open_deal', 'send_close_market_order', 'get_closed_deals',
    'get_limit_stop_orders', 'send_limit_order', 'send_limit_order_with_bracket', 'change_limit_order', 'change_limit_order_with_bracket',
    'cancel_limit_order', 'send_stop_order', 'send_stop_order_with_bracket', 'change_stop_order', 'change_stop_order_with_bracket',
    'cancel_stop_order', 'get_user_trading_signals', 'get_autochartist_signals', 'get_autochartist_chart_patterns', 'get_autochartist_fibonacci',
    'get_autochartist_key_levels', 'get_stocks_info', 'get_closed_binary_options', 'get_open_binary_options', 'send_buy_binary_option_order',
    'send_sell_binary_option_order', 'duplicate_open_binary_option', 'subscribe_options', 'get_user_local_settings', 'set_user_local_settings', 'subscribe_option_buy_out', 'unsubscribe_option_buy_out'
]);
export const getMockKesyEventLevel = () => ([
    'user_settings_outdated', 'trading_server_connection_lost', 'deal_opened_by_market_order', 'open_deal_changed', 'deal_closed_by_market_order',
    'deal_closed_by_bracket_order', 'deal_opened_by_limit_order', 'deal_opened_by_stop_order', 'deal_closed_by_tradeout_process', 'limit_order_submitted',
    'limit_order_changed', 'limit_order_canceled', 'stop_order_submitted', 'stop_order_changed', 'stop_order_canceled',
    'open_market_order_expired', 'close_market_order_expired', 'limit_order_expired', 'stop_order_expired', 'open_market_order_rejected',
    'close_market_order_rejected', 'limit_order_rejected', 'stop_order_rejected', 'favorites_security_added', 'favorites_security_removed',
    'deals_closed_by_tradeout', 'binary_option_bought_by_order', 'binary_option_sold_by_order', 'buy_binary_option_order_rejected',
    'sell_binary_option_order_rejected', 'binary_option_expired', 'user_account_outdated', 'unsubscribe_option_buy_out',
]);

export const getFullParser = () => {
    let parser = {};
    getMockKesyUpperLevel().map(key => {
        switch (key){
            case 'update': getMockKesyEventLevel().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({type: ev, status: true}) }); break;
            case 'response': getMockKesyResponseLevel().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({type: ev, status: true}) }); break;
            case 'event': getMockKesyUpdateLevel().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({type: ev, status: true}) }); break;
        }
    });
    return parser;
};

export const getMockKesyUpdateLevelConnector = () => (['get_trading_status', 'crm_single_sign_on']);
export const getMockKesyResponseLevelConnector = () => (['logout', 'get_trading_status']);
export const getMockKesyEventLevelConnector = () => (['trading_server_connection_lost']);

export const getConnectorParser = () => {
    let parser = {};
    getMockKesyUpperLevel().map(key => {
        switch (key){
            case 'update': getMockKesyUpdateLevelConnector().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({ type: ev, status: true }) }); break;
            case 'response': getMockKesyResponseLevelConnector().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({ type: ev, status: true }) }); break;
            case 'event': getMockKesyEventLevelConnector().map(ev => parser[key] = { ...parser[key], [ev]: ()=>({ type: ev, status: true }) }); break;
        }
    });
    return parser;
};

export const getConnectorObjectKeys = () => ({
    ...getMockKesyUpdateLevelConnector().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
    ...getMockKesyResponseLevelConnector().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
    ...getMockKesyEventLevelConnector().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
});

export const getMockKeysUpdateLevelPlatform = () => (['get_time', 'get_user_rates', 'get_user_statistics']);
export const getMockKeysResponseLevelPlatform = () => (['get_user_statistics', 'get_user_rates', 'get_time', 'get_user_account', 'get_user_settings']);
export const getMockKeysEventLevelPlatform = () => (['user_account_outdated', 'user_settings_outdated']);

export const getPlatformObjectKeys = () => ({
    ...getMockKeysUpdateLevelPlatform().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
    ...getMockKeysResponseLevelPlatform().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
    ...getMockKeysEventLevelPlatform().reduce((acc, key) => ({
        ...acc,
        [key.toUpperCase()]: ()=>({ type: key, status: true })
    }), {}),
});