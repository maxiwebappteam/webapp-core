'use strict';
export let brand = 'Superbinary';
let brandsConfig = {
    uTrader: {
        version: '1.1',
        loginservice: 'http://localhost:8096/auth/',
        keepaliveservice: 'http://localhost:8096/keepalive/',
        sockjspath: 'http://localhost:8096/stomp/',
        accountpage: {
            pl: 'https://pl.utrader.com/userdetails.html',
            en: 'https://en.utrader.com/userdetails.html',
            ru: 'https://ru.utrader.com/userdetails.html',
        },
        depositPage: {
            pl: 'https://pl.utrader.com/directdeposit.html',
            en: 'https://en.utrader.com/directdeposit.html',
            ru: 'https://ru.utrader.com/directdeposit.html',
        },
        title: 'uTrader',
        favIcon: 'assets/favicon-utrade.png',
        bigLogoSrc: {
            darkTheme: 'assets/logo-dlogin-utrade-dark.svg',
            lightTheme: 'assets/logo-dlogin-utrade-light.svg'
        },
        smallLogoHeader: {
            darkTheme: 'assets/logo-utrade-dark.svg',
            lightTheme: 'assets/logo-utrade-light.svg'
        },
        sitelangs: ['en', 'pl', 'ru'],
        loginLang: 'en',
        loginPrefix: 'utrader..',
        isNeedPack: true,
        googleTagManager: "<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-K5HF59\"height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-K5HF59');</script>",
        isOnlineChat: true,
        jivoWidgetId: {
            en: 'EXSjU7A8qi',
            ru: 'ZaAC0DlnBQ',
            pl: 'Ku8aVzyZWG',
        },
        isEnChatDisabled: false,
        logoutApiURL: null,
        themes: [
            'darkTheme',
            'lightTheme',
        ],
        externalLinks: {
            instrumentsForTrading: {
                ru: {
                    newsOfDay: 'https://ru.utrader.com/tata-motors-%D0%B2%D1%8B%D0%BF%D1%83%D1%81%D1%82%D1%8F%D1%82-%D1%81%D0%B0%D0%BC%D1%8B%D0%B9-%D0%B4%D0%B5%D1%88%D0%B5%D0%B2%D1%8B%D0%B9-%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BA%D0%B0%D1%80.html',
                    economicCalendar: 'https://ru.utrader.com/calendar.html',
                    tradingHours: 'https://ru.utrader.com/%D1%82%D0%BE%D1%80%D0%B3%D0%BE%D0%B2%D1%8B%D0%B5-%D1%87%D0%B0%D1%81%D1%8B.html',
                    dailyMarketReview: 'https://ru.utrader.com/%D1%84%D0%B8%D0%BD%D0%B0%D0%BD%D1%81%D0%BE%D0%B2%D1%8B%D0%B5-%D0%BE%D0%B1%D0%B7%D0%BE%D1%80%D1%8B.html',
                    financialNews: 'https://ru.utrader.com/%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8.html',
                    expiryRates: 'https://ru.utrader.com/expiryrates.html',
                    typesOfAccounts: 'https://ru.utrader.com/account-types.html'
                },
                en: {
                    economicCalendar: 'https://en.utrader.com/economic-calendar.html',
                    tradingHours: 'https://en.utrader.com/trading-hours.html',
                    dailyMarketReview: 'https://en.utrader.com/market-reviews-and-strategies.html',
                    financialNews: 'https://en.utrader.com/news.html',
                    typesOfAccounts: 'https://en.utrader.com/account-types.html',
                },
                pl: {
                    economicCalendar: 'https://pl.utrader.com/kalendarz-ekonomiczny.html ',
                    tradingHours: 'https://pl.utrader.com/godziny-handlu.html',
                    dailyMarketReview: 'https://pl.utrader.com/badania-i-strategie-rynkowe.html',
                    typesOfAccounts: 'https://pl.utrader.com/rodzaje-kont-i-oferty-bonusowe.html ',
                },
            },
            training: {
                ru: {
                    webinars: 'https://ru.utrader.com/webinar.html',
                    ourAnalyst: 'https://ru.utrader.com/%D0%BD%D0%B0%D1%88%D0%B8-%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D1%82%D0%B8%D0%BA%D0%B8.html',
                    videoLessons: 'https://ru.utrader.com/iu.html',
                    howToTrade: 'https://ru.utrader.com/%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D1%8B%D0%B5-%D0%BE%D0%BF%D1%86%D0%B8%D0%BE%D0%BD%D1%8B.html',
                    platformEbook: 'https://ru.utrader.com/platform-ebook.html',
                    glossaryOfTerms: 'https://ru.utrader.com/%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D1%8B%D0%B5-%D0%BE%D0%BF%D1%86%D0%B8%D0%BE%D0%BD%D1%8B-%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D1%80%D1%8C-%D1%82%D0%B5%D1%80%D0%BC%D0%B8%D0%BD%D0%BE%D0%B2.html',
                    faq: 'https://ru.utrader.com/%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B-%D0%B8-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%8B.html',
                    cryptocurrencySurvey: 'https://ru.utrader.com/%D0%BE%D0%BF%D1%80%D0%BE%D1%81-%D0%BF%D0%BE-%D0%BA%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D0%B2%D0%B0%D0%BB%D1%8E%D1%82%D0%B5.html',
                },
                en: {
                    webinars: 'https://en.utrader.com/webinar.html',
                    ourAnalyst: 'https://en.utrader.com/the-top-utraders-analysts.html ',
                    videoLessons: 'https://en.utrader.com/beginners.html ',
                    platformEbook: 'https://en.utrader.com/platform-ebook.html',
                    glossaryOfTerms: 'https://en.utrader.com/binary-options-glossary.html ',
                    faq: 'https://en.utrader.com/faq.html ',
                    cryptocurrencySurvey: 'https://en.utrader.com/cryptocurrency-survey.html',
                },
                pl: {
                    webinars: 'https://pl.utrader.com/webinar.html',
                    ourAnalyst: 'https://pl.utrader.com/najlepsi-analitycy-utrader.html',
                    videoLessons: 'https://pl.utrader.com/lessons-v.o.d.html',
                    glossaryOfTerms: 'https://pl.utrader.com/opcje-binarne-s%C5%82ownik-poj%C4%99%C4%87.html ',
                    faq: 'https://pl.utrader.com/faq.html',
                    cryptocurrencySurvey: 'https://pl.utrader.com/kwestionariusz-dotycz%C4%85cy-kryptowalut.html',
                },
            }
        },
        sharesLinks: {
            en: {
                riskFreeTransactions: 'https://en.utrader.com/utrader-insurance.html',
                educationalPackage: 'https://en.utrader.com/educational-package.html',
                turnover: 'https://en.utrader.com/get-paid-for-the-turnover.html',
                portfolioTrading: 'https://en.utrader.com/portfolio-trading.html',
            },
            pl: {
                riskFreeTransactions: 'https://pl.utrader.com/ubezpieczenie.html',
                educationalPackage: 'https://pl.utrader.com/pakiet-szkoleniowy.html',
                turnover: 'https://pl.utrader.com/otrzymaj-pieni%C4%85dze-za-obr%C3%B3t.html',
                inviteFriend: 'https://pl.utrader.com/zapro%C5%9B-znajomego.html',
                portfolioTrading: 'https://pl.utrader.com/handel-portfelem.html',
                potapov: 'https://pl.utrader.com/szkolenie-z-przemys%C5%82awem-dru%C5%BCbackim.html',
            },
            ru: {
                riskFreeTransactions: 'https://ru.utrader.com/%D1%81%D1%82%D1%80%D0%B0%D1%85%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B4%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%B0.html',
                educationalPackage: 'https://ru.utrader.com/%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9-%D0%BF%D0%B0%D0%BA%D0%B5%D1%82.html',
                turnover: 'https://ru.utrader.com/%D0%B4%D0%B5%D0%BD%D1%8C%D0%B3%D0%B8-%D0%B7%D0%B0-%D0%BE%D0%B1%D0%BE%D1%80%D0%BE%D1%82.html',
                inviteFriend: 'https://ru.utrader.com/%D0%BF%D1%80%D0%B8%D0%B3%D0%BB%D0%B0%D1%81%D0%B8-%D0%B4%D1%80%D1%83%D0%B3%D0%B0.html',
                portfolioTrading: 'https://ru.utrader.com/%D0%BF%D0%BE%D1%80%D1%82%D1%84%D0%B5%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D1%82%D0%BE%D1%80%D0%B3%D0%BE%D0%B2%D0%BB%D1%8F.html',
                potapov: 'https://ru.utrader.com/%D0%B2%D0%B8%D0%BF-%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5.html',
            },
        },
        helpLinksText: {
            en: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support.en@utrader.com',
                        emailLink: 'support.en@utrader.com'
                    }],
                },
                skypeLinkHelp: {
                    name: 'Skype',
                    skype: [{
                        text: 'utrader.helpdesk',
                        skypeLink: 'utrader.helpdesk'
                    }],
                },
                phonesLinkHelp: {
                    name: 'Phones',
                    phone: [
                        {
                            text: '+44 (208) 077-30-96 Great Britain',
                            phoneLink: '+442080773096'
                        },
                        {
                            text: '+883 (51000) 821-16-39',
                            phoneLink: '+883510008211639'
                        },
                    ],
                },
            },
            pl: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support.pl@utrader.com',
                        emailLink: 'support.pl@utrader.com'
                    }],
                },
                skypeLinkHelp: {
                    name: 'Skype',
                    skype: [{
                        text: 'utrader.pl',
                        skypeLink: 'utrader.pl'
                    }],
                },
                phonesLinkHelp: {
                    name: 'Telefony',
                    phone: [{
                        text: '+48 (22) 128-49-87 Polska',
                        phoneLink: '+48221284987'
                    }],
                },
            },
            ru: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support@utrader.com',
                        emailLink: 'support@utrader.com'
                    }],
                },
                skypeLinkHelp: {
                    name: 'Skype',
                    skype: [{
                        text: 'utrader.helpdesk',
                        skypeLink: 'utrader.helpdesk'
                    }],
                },
                phonesLinkHelp: {
                    name: 'Телефоны:',
                    phone: [
                        {
                            text: '+7 (495) 585-14-38 Россия',
                            phoneLink: '+74955851438'
                        },
                        {
                            text: '+7 (717) 272-74-43 Казахстан',
                            phoneLink: '+77172727443'
                        },
                        {
                            text: '+38 (094) 711-70-77 Украина',
                            phoneLink: '+380947117077'
                        },
                    ]
                },
            },
        },
        accountIframe: true,
        depositIframe: true,
        isKeepAlive: true,
        keepaliveTimeOut: 30000,
        validateEMail: false,
        crmApiUrl: 'https://test-api.umarkets.com',
        economicEventsLink: 'https://news.maximarkets.org/events',
        features: {
            demoTrendLine: true,
            economicEvents: true
        },
    },
    Superbinary: {
        version: '1.1',
        loginservice: 'http://localhost:8096/auth/',
        keepaliveservice: 'http://localhost:8096/keepalive/',
        sockjspath: 'http://localhost:8096/stomp/',
        accountpage: {
            ru: 'https://new.superbinary.com/userdetails',
            en: 'https://new.superbinary.com/userdetails',
            pl: 'https://new.superbinary.com/userdetails',
        },
        depositPage: {
            ru: 'https://new.superbinary.com/directdeposit',
            en: 'https://new.superbinary.com/directdeposit',
            pl: 'https://new.superbinary.com/directdeposit',
        },
        title: 'Superbinary',
        favIcon: 'assets/favicon-SuperBinary.png',
        // homePage: 'https://www.superbinary.com/',
        homePage: 'https://redux.js.org/',
        bigLogoSrc: {
            darkTheme: 'assets/logo-dlogin-superbinary.svg',
            lightTheme: 'assets/logo-dlogin-superbinary.svg'
        },
        smallLogoHeader: {
            // darkTheme: 'assets/logo-superbinary.svg',
            darkTheme: 'assets/crypto-redux.jpg',
            lightTheme: 'assets/logo-superbinary.svg'
        },
        sitelangs: ['en', 'pl', 'ru'],
        loginLang: 'ru',
        loginPrefix: 'sb..',
        isNeedPack: true,
        googleTagManager: "<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-K5HF59\"height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-K5HF59');</script>",
        isOnlineChat: true,
        jivoWidgetId: {
            en: 'Y54WzWDs9Y',
            ru: 'Y54WzWDs9Y',
            pl: 'Y54WzWDs9Y',
        },
        isEnChatDisabled: false,
        logoutApiURL: null,
        themes: [
            'darkTheme',
            'lightTheme',
        ],
        defaultTheme: 'darkTheme',
        externalLinks: {
            instrumentsForTrading: {
                ru: {
                    weeklyForecast: 'https://www.superbinary.com/weekly-forecast.html',
                    economicCalendar: 'https://superbinary.com/calendar.html',
                    tradingHours: 'https://superbinary.com/trading-hours.html',
                    dailyMarketReview: 'https://www.superbinary.com/daily-market-review-page.html',
                    strategies: 'https://www.superbinary.com/strategies.html',
                    typesOfAccounts: 'https://superbinary.com/account-types.html'
                },
                en: {
                    economicCalendar: 'https://superbinary.com/calendar.html',
                    tradingHours: 'https://superbinary.com/trading-hours.html',
                    typesOfAccounts: 'https://superbinary.com/account-types.html',
                },
                pl: {
                    economicCalendar: 'https://superbinary.com/calendar.html',
                    tradingHours: 'https://superbinary.com/trading-hours.html',
                    typesOfAccounts: 'https://superbinary.com/account-types.html',
                },
            },
            training: {
                ru: {
                    webinars: 'https://www.superbinary.com/%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D1%8B-%D0%BE%D1%82-superbinary/',
                    videoLessons: 'https://www.superbinary.com/education/#video',
                    education: 'https://www.superbinary.com/education.html',
                    faq: 'https://superbinary.com/faq-page.html',
                },
                en: {
                    webinars: 'https://www.superbinary.com/%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D1%8B-%D0%BE%D1%82-superbinary/',
                    videoLessons: 'https://www.superbinary.com/education/#video',
                    faq: 'https://superbinary.com/faq-page.html',
                },
                pl: {
                    webinars: 'https://www.superbinary.com/%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D1%8B-%D0%BE%D1%82-superbinary/',
                    videoLessons: 'https://www.superbinary.com/education/#video',
                    faq: 'https://superbinary.com/faq-page.html',
                },
            }
        },
        sharesLinks: {
            en: {
                riskFreeTransactions: 'https://www.superbinary.com/about-us/deposit-insurance/',
            },
            pl: {
                riskFreeTransactions: 'https://www.superbinary.com/about-us/deposit-insurance/',
            },
            ru: {
                riskFreeTransactions: 'https://www.superbinary.com/about-us/deposit-insurance/',
            },
        },
        helpLinksText: {
            en: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support@superbinary.com',
                        emailLink: 'support@superbinary.com'
                    }],
                },
                skypeLinkHelp: {
                    name: '⸮skype',
                    skype: []
                },
                phonesLinkHelp: {
                    name: 'Phones',
                    phone: [
                        {
                            text: '+7 (499) 350-70-36 Moscow',
                            phoneLink: '+74993507036'
                        },
                        {
                            text: '+44 (203) 957–85–05 London',
                            phoneLink: '+442039578505'
                        },
                        {
                            text: '+7 (717) 272–79–31 Astana',
                            phoneLink: '+77172727931'
                        },
                        {
                            text: '+38 (044) 379–48–32 Kiev',
                            phoneLink: '+380443794832'
                        },
                    ]
                },
            },
            pl: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support@superbinary.com',
                        emailLink: 'support@superbinary.com'
                    }],
                },
                skypeLinkHelp: {
                    name: '⸮skype',
                    skype: []
                },
                phonesLinkHelp: {
                    name: 'Telefony',
                    phone: [
                        {
                            text: '+7 (499) 350-70-36 Moskwa',
                            phoneLink: '+74993507036'
                        },
                        {
                            text: '+44 (203) 957–85–05 London',
                            phoneLink: '+442039578505'
                        },
                        {
                            text: '+7 (717) 272–79–31 Astana',
                            phoneLink: '+77172727931'
                        },
                        {
                            text: '+38 (044) 379–48–32 Kijów',
                            phoneLink: '+380443794832'
                        },
                    ]
                },
            },
            ru: {
                emailLinkHelp: {
                    name: 'Email',
                    email: [{
                        text: 'support@superbinary.com',
                        emailLink: 'support@superbinary.com'
                    }]
                },
                skypeLinkHelp: {
                    name: '⸮skype',
                    skype: []
                },
                phonesLinkHelp: {
                    name: 'Телефоны:',
                    phone: [
                        {
                            text: '+7 (499) 350-70-36 Москва',
                            phoneLink: '+74993507036'
                        },
                        {
                            text: '+44 (203) 957–85–05 Лондон',
                            phoneLink: '+442039578505'
                        },
                        {
                            text: '+7 (717) 272–79–31 Астана',
                            phoneLink: '+77172727931'
                        },
                        {
                            text: '+38 (044) 379–48–32 Киев',
                            phoneLink: '+380443794832'
                        },
                    ]
                },
            },
        },
        accountIframe: true,
        depositIframe: true,
        isKeepAlive: true,
        keepaliveTimeOut: 30000,
        validateEMail: false,
        crmApiUrl: 'https://test-api.umarkets.com',
        economicEventsLink: 'https://news.maximarkets.org/events',
        features: {
            demoTrendLine: true,
            economicEvents: true
        },
    },
    VXmarkets : {
        version: '1.1',
        loginservice: 'http://localhost:8096/auth/',
        keepaliveservice: 'http://localhost:8096/keepalive/',
        sockjspath: 'http://localhost:8096/stomp/',
        accountpage: {
            en: "https://vxmarkets.com/userdetails/",
        },
        depositPage: {
            en: "https://vxmarkets.com/directdeposit/",
        },
        title: 'VXmarkets',
        favIcon: 'assets/favicon-vxmarkets.ico',
        bigLogoSrc: {
            darkTheme: "assets/logo-dlogin-vxmarkets.png",
            lightTheme: "assets/logo-dlogin-vxmarkets.png"
        },
        smallLogoHeader: {
            darkTheme: "assets/logo-vxmarkets.svg",
            lightTheme: "assets/logo-vxmarkets.svg"
        },
        sitelangs: ['en'],
        loginLang: 'en',
        loginPrefix: 'vxmarkets..',
        isNeedPack: true,
        googleTagManager: "<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-K5HF59\"height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-K5HF59');</script>",
        isOnlineChat: true,
        jivoWidgetId: {
            en: "Q6BnLbl1eD",
        },
        isEnChatDisabled: false,
        logoutApiURL: null,
        themes: [
            'darkTheme',
            'lightTheme',
        ],
        externalLinks: null,
        sharesLinks: null,
        helpLinksText: {
            en: {
                emailLinkHelp: {name:"Email" ,email: [{text:"helpdesk@vxmarkets.com", emailLink: "helpdesk@vxmarkets.com"}],},
                skypeLinkHelp: {name:"Skype" ,skype: []},
                phonesLinkHelp: {name:"Phones" ,phone: [
                        {text:"+44 (203) 129-9845 London", phoneLink: "+442031299845"},
                    ]},
            },
        },
        accountIframe: true,
        depositIframe: true,
        isKeepAlive: true,
        keepaliveTimeOut: 30000,
        validateEMail: false,
        crmApiUrl: 'https://test-api.umarkets.com',
        economicEventsLink: 'https://news.maximarkets.org/events',
        features: {
            demoTrendLine: true,
            economicEvents: true
        },
    },
};

export let configSite = brandsConfig[brand];