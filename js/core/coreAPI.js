import connectorReducer from './connector/reducer';
import coreConnectorSaga from './connector/saga';
import { combine } from './connector/parser/Parser';
import coreParser from './connector/parser/coreParser';
import { initRequestSender } from './connector/requestSender/RequestSender';
import { initParsers } from './connector/parser/Parser';
import { combineRequest } from './connector/requestSender/RequestSender';
import coreRequestSender from './connector/requestSender/coreRequestSender';
import { initConnection } from './connector/actions';

export const coreReducer = {
    sessionData: connectorReducer,
};

export const coreSaga = [
    coreConnectorSaga,
];

export const combineParsers = parsers => {
    if (!parsers) {
        parsers = {};
    }
    return combine({
        coreParser,
        parsers,
    });
};

export const combineRequestSenders = requestSender => {
    if (!requestSender) {
        requestSender = {};
    }
    return combineRequest({
        coreRequestSender,
        requestSender,
    });
};

export const initCore = (rootParser, rootRequestSenders) => {
    rootParser && initParsers(rootParser);
    rootRequestSenders && initRequestSender(rootRequestSenders);
};

export const startConnection = (dispatch) => {
    dispatch && dispatch(initConnection());
};
