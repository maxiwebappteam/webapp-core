const constants = {};

export default Object.defineProperties(constants, {
    // ToDo BASE constants
    INIT_CONNECTION                 : { value: 'init_connection', writable: false },
    CONNECT                         : { value: 'connect', writable: false },
    CONNECT_WS                      : { value: 'connect_ws', writable: false },
    CONNECT_WS_SUCCESS              : { value: 'connect_ws_success', writable: false },
    CONNECT_WS_MESSAGE              : { value: 'connect_ws_message', writable: false },
    CONNECT_WS_ERROR                : { value: 'connect_ws_error', writable: false },
    SET_SIDS_STORE                  : { value: 'set_sids_store', writable: false },

    LOGOUT                          : { value: 'logout', writable: false },

    GET_USER_STATISTICS             : { value: 'get_user_statistics', writable: false },
    SET_USER_STATISTICS             : { value: 'set_user_statistics', writable: false },
    SET_USER_STATISTICS_STORE       : { value: 'set_user_statistics_store', writable: false },

    INITIALIZE_TRADING_UNIT         : { value: 'initialize_trading_unit', writable: false },

    GET_USER_SETTINGS               : { value: 'get_user_settings', writable: false },
    SET_USER_SETTINGS               : { value: 'set_user_settings', writable: false },
    SET_USER_SETTINGS_STORE         : { value: 'set_user_settings_store', writable: false },

    GET_USER_RATES                  : { value: 'get_user_rates', writable: false },
    SET_USER_RATES                  : { value: 'set_user_rates', writable: false },

    UPDATE_LOADED_RATES_STORE       : { value: 'update_loaded_rates_store', writable: false },
    SET_LOADED_RATES_STORE          : { value: 'set_loaded_rates_store', writable: false },

    KEEP_ALIVE_REQUEST              : { value: 'keep_alive_request', writable: false },
    KEEP_ALIVE_REQUEST_SUCCESS      : { value: 'keep_alive_request_success', writable: false },
    KEEP_ALIVE_REQUEST_FAILURE      : { value: 'keep_alive_request_failure', writable: false },

    RECONNECT_REQUEST               : { value: 'reconnect_request', writable: false },
    RECONNECT_REQUEST_SUCCESS       : { value: 'reconnect_request_success', writable: false },
    RECONNECT_REQUEST_FAILURE       : { value: 'reconnect_request_failure', writable: false },

    SET_SESSION_DATA_STORE          : { value: 'set_session_data_store', writable: false },
    SET_CONNECTION_STATE_STORE      : { value: 'set_connection_state_store', writable: false },
    CHANGE_LANGUAGE                 : { value: 'change_language', writable: false },
    CHANGE_ACCOUNT                  : { value: 'change_account', writable: false },
    CHANGE_SITE_THEME               : { value: 'change_site_theme', writable: false },

    CRM_SSO_REQUEST                 : { value: 'crm_sso_request', writable: false },
    CRM_SSO_REQUEST_SUCCESS         : { value: 'crm_sso_request_success', writable: false },
    CRM_SSO_REQUEST_FAILURE         : { value: 'crm_sso_request_failure', writable: false },

    SET_CRM_SSO_DATA                : { value: 'set_crm_sso_data', writable: false },

    // ToDo constants from CONNECTOR
    GET_USER_ACCOUNT                : { value: 'get_user_account', writable: false },
    SET_USER_ACCOUNT                : { value: 'set_user_account', writable: false },
    SET_USER_CURRENCY_STORE         : { value: 'set_user_currency_store', writable: false },
    USER_ACCOUNT_OUTDATED           : { value: 'user_account_outdated', writable: false },

    GET_TIME                        : { value: 'get_time', writable: false },
    SET_TIME                        : { value: 'set_time', writable: false },
    SET_TIME_STORE                  : { value: 'set_time_store', writable: false },

    GET_TRADING_STATUS              : { value: 'get_trading_status', writable: false },
    GET_STOCKS_INFO                 : { value: 'get_stocks_info', writable: false },
    CRM_SINGLE_SIGN_ON              : { value: 'crm_single_sign_on', writable: false },
    USER_SETTINGS_OUTDATED          : { value: 'user_settings_outdated', writable: false },
    TRADING_SERVER_CONNECTION_LOST  : { value: 'trading_server_connection_lost', writable: false },
    LS_SET_ITEM_SIDS                : { value: 'ls_set_item_sids', writable: false },
    LS_SET_ITEM_SESSION_TID         : { value: 'ls_set_item_session_tid', writable: false },
    SEND_REQUEST                    : { value: 'send_request', writable: false },
    WINDOW_UNLOAD                   : { value: 'window_unload', writable: false },

    ERROR                           : { value: 'error', writable: false },
});
