import { convertMessageToBytes, decodeMessage } from '../decodeMessage';

const mockMessage = 'hKExqGdldF90aW1loTKmdXBkYXRloTSBoTPPAAABZThz45ChbYShMaNjbWShMqR0eXBloTOkdGltZaE0p2NvbnRlbnQ=';
const mockBytes = [132, 161, 49, 168, 103, 101, 116, 95, 116, 105, 109, 101, 161, 50, 166,
    117, 112, 100, 97, 116, 101, 161, 52, 129, 161, 51, 207, 0, 0, 1, 101, 56, 115, 227, 144, 161, 109, 132, 161, 49, 163, 99, 109, 100, 161, 50, 164, 116, 121, 112, 101, 161, 51, 164, 116, 105, 109,
    101, 161, 52, 167, 99, 111, 110, 116, 101, 110, 116, 111, 84, 79, 107, 100, 71, 108, 116, 90, 97, 69, 48, 112, 50, 78, 118, 98, 110, 82, 108, 98, 110, 81, 61];

describe('coreUtils   convertMessageToBytes', () => {
    let sandbox;
    before(()=>{
        sandbox = sinon.createSandbox();
    });

    it('convertMessageToBytes should return bytes', () => {
        const act = convertMessageToBytes(mockMessage);
        const exp = mockBytes;
        assert.deepEqual(act, exp);
    });

    it('convertMessageToBytes should catch error', () => {
        const error = sandbox.spy(global.console, 'error');
        const message = '@!#$%^&*()';
        convertMessageToBytes(message);
        sinon.assert.called(error);
    });

    afterEach(()=>{
        sandbox.restore();
    });
});

describe('coreUtils   decodeMessage', () => {
    let sandbox;
    before(()=>{
        sandbox = sinon.createSandbox();
    });

    it('decodeMessage   when window location search isn\'t empty', () => {
        const unpacked = {
            1: 'get_user_rates',
            2: 'update',
            10: {
                8: [
                    { 3: 'DOWUSD', 4: 1531320130065, 5: 24780780000, 6: 24783680000, 7: 1.082851519879266 },
                    { 3: 'AUDCHF', 4: 1531320129917, 5: 734480, 6: 734800, 7: -0.15493761722253935 },
                    { 3: 'GBPCAD', 4: 1531320129964, 5: 1735070, 6: 1735700, 7: -0.3279545343591246 },
                    { 3: 'AUDCAD', 4: 1531320129917, 5: 969110, 6: 969440, 7: -0.4769385575817316 },
                    { 3: 'SGDJPY', 4: 1531320129485, 5: 81809000, 6: 81866000, 7: 0.5405571424183789 }
                ],
                9: []
            },
            m: {
                1: 'cmd',
                2: 'type',
                3: 's',
                4: 't',
                5: 'bp',
                6: 'ap',
                7: 'dc',
                8: 'rates',
                9: 'bars',
                10: 'content',
            }
        };
        const act = decodeMessage(unpacked);
        const exp = {
            cmd: 'get_user_rates',
            content: {
                bars: [],
                rates: [
                    { s: 'DOWUSD', t: 1531320130065, bp: 24780780000, ap: 24783680000, dc: 1.082851519879266 },
                    { s: 'AUDCHF', t: 1531320129917, bp: 734480, ap: 734800, dc: -0.15493761722253935 },
                    { s: 'GBPCAD', t: 1531320129964, bp: 1735070, ap: 1735700, dc: -0.3279545343591246 },
                    { s: 'AUDCAD', t: 1531320129917, bp: 969110, ap: 969440, dc: -0.4769385575817316 },
                    { s: 'SGDJPY', t: 1531320129485, bp: 81809000, ap: 81866000, dc: 0.5405571424183789 }
                ],
            },
            type: 'update'
        };
        assert.deepEqual(act, exp);
    });

    it('decodeMessage should catch error', () => {
        const error = sandbox.spy(global.console, 'error');
        decodeMessage();
        sinon.assert.called(error);
    });

    afterEach(()=>{
        sandbox.restore();
    });
});
