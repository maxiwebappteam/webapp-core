import { getCookie } from '../getCookie';

describe('coreUtils   getCookie', () => {
    it('getCookie   when parameter \'um_session\' is transferred', () => {
        document.cookie = 'um_session=94a453f103c44730a57a79a07857cb76; jv_refer_Y54WzWDs9Y=http%3A%2F%2Flocalhost%3A8096%2Flogin.html%3Fcode%3D404%26lang%3Dru; jv_enter_ts_Y54WzWDs9Y=1531293294221; jv_visits_count_Y54WzWDs9Y=6; jv_utm_Y54WzWDs9Y=; jv_pages_count_Y54WzWDs9Y=20';
        const act = getCookie('um_session');
        const exp = '94a453f103c44730a57a79a07857cb76';
        assert.equal(act, exp);
    });

    it('getCookie   when parameter \'um_session\' isn\'t transferred', () => {
        document.cookie = 'um_session=94a453f103c44730a57a79a07857cb76; jv_refer_Y54WzWDs9Y=http%3A%2F%2Flocalhost%3A8096%2Flogin.html%3Fcode%3D404%26lang%3Dru; jv_enter_ts_Y54WzWDs9Y=1531293294221; jv_visits_count_Y54WzWDs9Y=6; jv_utm_Y54WzWDs9Y=; jv_pages_count_Y54WzWDs9Y=20';
        const act = getCookie();
        const exp = void 0;
        assert.equal(act, exp);
    });
});
