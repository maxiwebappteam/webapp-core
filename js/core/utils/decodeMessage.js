import { stringToUtf8ByteArray } from 'utf8-string-bytes';

export function convertMessageToBytes(message) {
    try {
        const binary = window.atob(message.replace(/\s/g, ''));
        let len = binary.length;
        let bytes = stringToUtf8ByteArray(message);

        for (let i = 0; i < len; i++) {
            bytes[i] = binary.charCodeAt(i);
        }
        return bytes;
    } catch (e) {
        console.error(e);
    }
}

export function decodeMessage(unpacked) {
    try {
        let keys = unpacked.m;
        delete unpacked.m;
        return applyKeysToObject(unpacked, keys);
    } catch (e) {
        console.error(e);
    }

    function applyKeysToEntity(entity, keys) {
        if (typeof entity === 'object') {
            if (entity.length === void (0)) {
                entity = applyKeysToObject(entity, keys);
            } else {
                entity = applyKeysToArray(entity, keys);
            }
        }
        return entity;
    }

    function applyKeysToObject(src, keys) {
        let result = {};
        for (let key in src) {
            result[keys[key]] = applyKeysToEntity(src[key], keys);
        }
        return result;
    }

    function applyKeysToArray(src, keys) {
        let result = [];
        src.forEach(function (entity) {
            result.push(applyKeysToEntity(entity, keys));
        });
        return result;
    }
}
