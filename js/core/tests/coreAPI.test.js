import { initCore, startConnection, coreReducer, coreSaga, combineParsers, combineRequestSenders } from '../coreAPI';
import * as requestSender from '../connector/requestSender/RequestSender';
import * as parser from '../connector/parser/Parser';
import * as actions from '../connector/actions';
import connectorReducer from '../connector/reducer';
import coreConnectorSaga from "../connector/saga";
import coreRequestSender from '../connector/requestSender/coreRequestSender';
import coreParser from "../connector/parser/coreParser";

describe('coreAPI tests', () => {
    const initRequestSender = sinon.spy(requestSender, 'initRequestSender');
    const initParsers = sinon.spy(parser, 'initParsers');
    const initConnection = sinon.spy(actions, 'initConnection');
    const dispatch = sinon.spy();

    afterEach(()=>{
        initRequestSender.resetHistory();
        initParsers.resetHistory();
        initConnection.resetHistory();
        dispatch.resetHistory();
    });

    it('initCore should call initParsers & initRequestSender functions', () => {
        initCore({}, {});
        assert.equal(initParsers.called, true);
        assert.equal(initRequestSender.called, true);
    });

    it('initCore should call only initParsers', () => {
        initCore({});
        assert.equal(initParsers.called, true);
        assert.equal(initRequestSender.called, false);
    });

    it('initCore should call only initRequestSender', () => {
        initCore(null, {});
        assert.equal(initParsers.called, false);
        assert.equal(initRequestSender.called, true);
    });

    it('initCore no called functions', () => {
        initCore();
        assert.equal(initParsers.called, false);
        assert.equal(initRequestSender.called, false);
    });

    it('startConnection no called functions', () => {
        startConnection();
        assert.equal(initConnection.called, false);
        assert.equal(dispatch.called, false);
    });

    it('startConnection should call dispatch callback function', () => {
        startConnection(dispatch);
        assert.equal(initConnection.called, true);
        assert.equal(dispatch.called, true);
    });

    it('coreReducer should be object', () => {
        assert.deepEqual(coreReducer, {
            sessionData: connectorReducer,
        });
    });

    it('coreReducer should be list', () => {
        assert.deepEqual(coreSaga, [
            coreConnectorSaga,
        ]);
    });

    it('combineRequestSenders without args return combineRequest object', () => {
        assert.deepEqual(combineRequestSenders(), {
            ...coreRequestSender
        });
    });

    it('combineRequestSenders with args return combineRequest object', () => {
        const testData = {
            test: 'test'
        };

        assert.deepEqual(combineRequestSenders(testData), {
            ...coreRequestSender,
            ...testData
        });
    });

    it('combineParsers without args return combineParser object', () => {
        assert.deepEqual(combineParsers(), {
            ...coreParser,
        });
    });

    it('combineParsers with args return combineParser object', () => {
        const testData = {
            update: {
                test: 'test'
            }
        };

        assert.deepEqual(combineParsers(testData), {
            ...coreParser,
            update: {
                ...coreParser.update,
                ...testData.update
            }
        });
    });

    it('combineParsers with args and no changed return combineParser object', () => {
        const testData = {
            test: 'test'
        };

        assert.deepEqual(combineParsers(testData), {
            ...coreParser,
        });
    });
});
