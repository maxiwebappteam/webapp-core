export const getSockPath = state => state.configCore.sockjspath;
export const getSockUser = state => state.sessionData.su;
export const getLanguage = state => state.sessionData.lang;
export const getCRMApiUrl = state => state.configCore.crmApiUrl;
export const getSessionId = state => state.sessionData.sid;
export const getSessionIds = state => state.sessionData.sids;
export const getSessionData = state => state.sessionData;
export const getCustomMarker = state => state.configCore.customMarker;
export const getSockPassword = state => state.sessionData.sp;
export const getSessionTimeId = state => state.sessionData.tid;
export const isConnectionAlive = state => state.sessionData.isConnectAlive;
export const isLocalStorageSupport = state => state.userLocalSettings.isLocalStorageSupport;
export const getKeepAliveParams = state => ({
    keepAliveService: state.configCore.keepaliveservice,
    keepAliveTimeOut: state.configCore.keepaliveTimeOut,
    isKeepAlive: state.configCore.isKeepAlive,
});

export const getReconnectParams = state => ({
    loginService: state.configCore.loginservice,
    keepAliveTimeOut: state.configCore.keepaliveTimeOut,
});

// export const getUsersDropDown = state => getUsers(state).map(user => {
//     const { id, first_name, last_name, bo_user_photo } = user;
//     return {
//         id,
//         value: `${first_name ? first_name : ''} ${last_name ? last_name : ''}`.trim(),
//         picture: bo_user_photo
//     }
// });
