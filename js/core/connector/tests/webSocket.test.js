import * as webSocket from '../webSocket';
import * as selectors from '../../../mockData/mockSelectors';
import * as actions from '../../../mockData/mockActions';
import * as selectorsConnector from '../selectors';
import { call, take, put, select, apply } from 'redux-saga/effects';
import { Stomp } from '@stomp/stompjs';

const state = store.getState();
const sessionData = selectors.getSessionData(state);
const socketPath = selectors.getSockPath(state);
const mockWebSocket = {
    close: sinon.spy(),
};
const mockStopmClient = {
    send: sinon.spy(),
    connect: sinon.spy(),
    disconnect: sinon.spy(),
    subscribe: sinon.stub(),
    heartbeat: {},
};

const initMockStopmClient = () => {
    const generatorStopmClient = webSocket.initStompClient(socketPath);
    generatorStopmClient.next();
    generatorStopmClient.next(mockWebSocket);
    generatorStopmClient.next(mockStopmClient);
};

describe('webSocket -> closeStompClient', () => {
    it('closeStompClient should return false', () => {
        assert.isFalse(webSocket.closeStompClient());
    });

    it('closeStompClient should close stompClient and myWebSocket', () => {
        initMockStopmClient();
        webSocket.closeStompClient();
        sinon.assert.called(mockStopmClient.disconnect);
        sinon.assert.called(mockWebSocket.close);
        assert.isNull(webSocket.stompClient);
    });
});

describe('webSocket -> closeWebSocket', () => {
    let sandbox, mockChannel;

    before(() => {
        sandbox = sinon.createSandbox();
        mockChannel = {
            close: sandbox.stub(),
            take: sandbox.stub(),
        };
    });

    it('closeWebSocket should return false', () => {
        assert.isFalse(webSocket.closeWebSocket());
    });

    it('closeWebSocket should call close fun', () => {
        const generator = webSocket.connectWebSocket();
        generator.next();
        generator.next(sessionData);
        generator.next(socketPath);
        initMockStopmClient();
        generator.next();
        generator.next(mockChannel);
        webSocket.closeWebSocket();
        sandbox.assert.called(mockChannel.close);
    });

    afterEach(() => {
        webSocket.closeStompClient();
        sandbox.restore();
    });
});

describe('webSocket -> connectWebSocket', () => {
    const generatorConnectWebSocket = webSocket.connectWebSocket();
    const channel = {
        take: () => {},
        flush: () => {},
        close: () => {},
    };

    before(() => {
        initMockStopmClient();
    });

    it('connectWebSocket should return select getSessionData', () => {
        assert.deepEqual(generatorConnectWebSocket.next().value, select(selectorsConnector.getSessionData));
    });

    it('connectWebSocket should return select getSockPath', () => {
        assert.deepEqual(generatorConnectWebSocket.next(sessionData).value, select(selectorsConnector.getSockPath));
    });

    it('connectWebSocket should return call initStompClient', () => {
        assert.deepEqual(generatorConnectWebSocket.next(socketPath).value, call(webSocket.initStompClient, socketPath));
    });

    it('connectWebSocket should return call connectStompClient', () => {
        assert.deepEqual(generatorConnectWebSocket.next().value, call(webSocket.connectStompClient, sessionData.su, sessionData.sp, sessionData.sid));
    });

    it('connectWebSocket should return take channel', () => {
        assert.deepEqual(generatorConnectWebSocket.next(channel).value, take(channel));
    });

    it('connectWebSocket should return put eventAction', () => {
        const eventAction = 'test';
        assert.deepEqual(generatorConnectWebSocket.next(eventAction).value, put(eventAction));
    });

    it('connectWebSocket should return false', () => {
        const generator = webSocket.connectWebSocket();
        generator.next();
        generator.next(sessionData);
        generator.next(socketPath);
        generator.next();
        generator.next(channel);

        assert.isFalse(generator.next(null).value, false);
        assert.isTrue(generator.next().done);
    });

    after(() => {
        webSocket.closeStompClient();
    });
});

describe('webSocket -> onConnect', () => {
    let sandbox = sinon.createSandbox();
    const emitter = sandbox.spy();
    beforeEach(() => {
        initMockStopmClient(socketPath);
    });

    it('onConnect no emitter should return false', () => {
        assert.isFalse(webSocket.onConnect());
    });

    it('onConnect call stompClient.subscribe', () => {
        const sessionId = 'test';
        const error = sandbox.spy(global.console, 'error');
        mockStopmClient.subscribe.throws();
        webSocket.onConnect(emitter, sessionId);
        const args = mockStopmClient.subscribe.getCall(0).args;
        args[1]('test');
        assert.deepEqual(emitter.getCall(0).args[0], actions.connectWsSuccess());
        assert.deepEqual(emitter.getCall(1).args[0], actions.connectWsMessage(sessionId));
        assert.equal(args[0], `/amq/queue/session.${sessionId}`);
        sinon.assert.called(mockStopmClient.subscribe);
        sinon.assert.called(error);
    });

    afterEach(() => {
        webSocket.closeStompClient();
        sandbox.restore();
    });
});

describe('webSocket -> onError', () => {
    let sandbox;
    const emitter = sinon.spy();
    before(() => {
        sandbox = sinon.createSandbox();
    });

    it('onError no emitter should return false', () => {
        assert.isFalse(webSocket.onError());
    });

    it('onError call emitter with connectWsError action', () => {
        webSocket.onError(emitter);
        sinon.assert.calledWith(emitter, actions.connectWsError());
    });

    afterEach(() => {
        sandbox.restore();
    });
});

describe('webSocket -> initStompClient', () => {
    const generator = webSocket.initStompClient(socketPath);

    it('initStompClient should return call getNewSockJS', () => {
        assert.deepEqual(generator.next().value, call(webSocket.getNewSockJS, socketPath));
    });

    it('initStompClient should return apply Stomp.over', () => {
        assert.deepEqual(generator.next(mockWebSocket).value, apply(Stomp, Stomp.over, [mockWebSocket]));
    });

    it('initStompClient should finished', () => {
        assert.isTrue(generator.next(mockStopmClient).done);
    });

    it('initStompClient check params', () => {
        const act = {
            outgoing: webSocket.stompClient.heartbeat.outgoing,
            incoming: webSocket.stompClient.heartbeat.incoming,
        };
        const exp = {
            outgoing: 2000,
            incoming: 2000,
        };

        assert.deepEqual(act, exp);
    });

    after(() => {
        webSocket.closeStompClient();
    });
});

describe('webSocket -> sendRequest', () => {
    before(() => {
        initMockStopmClient();
    });

    it('sendRequest if no stompClient should return false', () => {
        const generator = webSocket.sendRequest();
        assert.isFalse(generator.next().value);
        assert.isTrue(generator.next().done);
    });

    it('sendRequest send', () => {
        const mock = 'test';
        const generator = webSocket.sendRequest(mock);

        assert.deepEqual(generator.next().value, apply(webSocket.stompClient, webSocket.stompClient.send, ['/exchange/CMD/', {}, mock]));
        assert.isTrue(generator.next().done);
    });

    after(() => {
        webSocket.closeStompClient();
    });
});

describe('webSocket -> connectStompClient', () => {
    let sandbox;
    before(() => {
        sandbox = sinon.createSandbox();
    });
    it('connectStompClient should call stompClient.connect', () => {
        initMockStopmClient();
        const channel = webSocket.connectStompClient(sessionData.su, sessionData.sp, sessionData.sid);
        const args = mockStopmClient.connect.getCall(0).args;

        channel.close();

        sinon.assert.called(mockStopmClient.connect);
        assert.equal(args[0], sessionData.su);
        assert.equal(args[1], sessionData.sp);
        assert.equal(args[args.length - 1], 'trading');
        assert.isNull(webSocket.stompClient);
    });

    it('connectStompClient should catch error', () => {
        webSocket.closeStompClient();
        const error = sandbox.spy(global.console, 'error');
        webSocket.connectStompClient(sessionData.su, sessionData.sp, sessionData.sid);
        sinon.assert.called(error);
    });

    afterEach(() => {
        webSocket.closeStompClient();
        sandbox.restore();
    });
});

describe('webSocket -> getNewSockJS', () => {
    it('getNewSockJS should return new Object', () => {
        const act = webSocket.getNewSockJS(socketPath);
        assert.isObject(act, 'object');
        act.close();
    });
});