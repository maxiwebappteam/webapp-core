import requestSender from '../requestSender/coreRequestSender';
import { getCoreRequestSender, getMockCoreRequestKeys } from '../../../mockData/mockRequestSenderData';
import sessionData from '../../../mockData/mockSessionData';

const mockUM_session = '980ea5e373e948548c45b30e9cd3d389';
const mockSessioId = sessionData.sid;
document.cookie = `um_session=${mockUM_session};`;
const coreRequestSenderObjectKeys = getCoreRequestSender();

describe('request sender', () => {
    it(`check command keeper keys`, () => {
        assert.deepEqual(Object.keys(requestSender), getMockCoreRequestKeys());
    });
});

describe('request sender methods', () => {
    for (let key in coreRequestSenderObjectKeys) {
        const action = coreRequestSenderObjectKeys[key]().type;

        it(`check ${key} function without input params and context`, () => {
            const act = JSON.parse(requestSender[action]());
            const exp = {
                umid: mockUM_session,
                cmd: action,
                params: [],
            };
            assert.deepEqual(act, exp);
        });

        it(`check ${key} function without input params`, () => {
            const sessionId = mockSessioId;
            const act = JSON.parse(requestSender[action]({ sessionId }));
            const exp = {
                sid: mockSessioId,
                umid: mockUM_session,
                cmd: action,
                params: [],
            };
            assert.deepEqual(act, exp);
        });

        it(`check ${key} function without input context`, () => {
            const act = JSON.parse(requestSender[action](undefined, []));
            const exp = {
                umid: mockUM_session,
                cmd: action,
                params: [],
            };
            assert.deepEqual(act, exp);
        });

        it(`check ${key} function with all params`, () => {
            const sessionId = mockSessioId;
            const act = JSON.parse(requestSender[action]({ sessionId }, ['test']));
            const exp = {
                sid: mockSessioId,
                umid: mockUM_session,
                cmd: action,
                params: ['test'],
            };
            assert.deepEqual(act, exp);
        });

        it(`check ${key} function with invalid context`, () => {
            const act = JSON.parse(requestSender[action]({ test: 'tests' }, []));
            const exp = {
                umid: mockUM_session,
                cmd: action,
                params: [],
            };
            assert.deepEqual(act, exp);
        });
    }
});
