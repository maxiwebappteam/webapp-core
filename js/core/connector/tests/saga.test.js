import { takeEvery, take, put, call, select } from 'redux-saga/effects';
import { requestPOST, requestGET } from '../restAPI';
import { assignLocation, getRemovedSid, intervalEventChannel } from '../saga';
import * as RequestSender from '../requestSender/RequestSender';
import { closeWebSocket, connectWebSocket, sendRequest } from '../webSocket';
import { parseResult } from '../parser/Parser';
import watchConnector, * as saga from '../saga';
import * as selectorsConnector from '../selectors';
import * as mockActions from '../../../mockData/mockActions';
import * as mockSelectors from '../../../mockData/mockSelectors';
import { getCurrentTime } from '../../utils/getCurrentTime';

const state = store.getState();
const sessionData = mockSelectors.getSessionData(state);
const keepAliveParams = mockSelectors.getKeepAliveParams(state);
const getLogoutCode = code => `code=${code}&lang=${sessionData.lang}`;
const customMarker = mockSelectors.getCustomMarker(state);
const isLocalStorageSupport = mockSelectors.isLocalStorageSupport(state);

describe('connector saga watchConnector', () => {
    const generatorWatchConnector = watchConnector();

    it('watchConnector should takeEvery init', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.initConnection().type, saga.init));
    });

    it('watchConnector should takeEvery connect', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.connect().type, saga.connect));
    });

    it('watchConnector should takeEvery connectWebSocket', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.connectWs().type, connectWebSocket));
    });

    it('watchConnector should takeEvery connectWebSocketSuccess', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.connectWsSuccess().type, saga.connectWebSocketSuccess));
    });

    it('watchConnector should takeEvery parseResult', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.connectWsMessage().type, parseResult));
    });

    it('watchConnector should takeEvery connectWebSocketError', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.connectWsError().type, saga.connectWebSocketError));
    });

    it('watchConnector should takeEvery logout', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.logout().type, saga.logout));
    });

    it('watchConnector should takeEvery keepAliveRequest', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.keepAliveRequest().type, saga.keepAliveRequest));
    });

    it('watchConnector should takeEvery keepAliveRequestSuccess', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.keepAliveRequestSuccess().type, saga.keepAliveRequestSuccess));
    });

    it('watchConnector should takeEvery keepAliveRequestFailure', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.keepAliveRequestFailure().type, saga.keepAliveRequestFailure));
    });

    it('watchConnector should takeEvery reconnectRequest', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.reconnectRequest().type, saga.reconnectRequest));
    });

    it('watchConnector should takeEvery reconnectRequestSuccess', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.reconnectRequestSuccess().type, saga.reconnectRequestSuccess));
    });

    it('watchConnector should takeEvery reconnectRequestFailure', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.reconnectRequestFailure().type, saga.reconnectRequestFailure));
    });

    it('watchConnector should takeEvery changeLanguage', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.changeLanguage().type, saga.changeLanguage));
    });

    it('watchConnector should takeEvery crmSSORequest', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.crmSSORequest().type, saga.crmSSORequest));
    });

    it('watchConnector should takeEvery crmSSORequestSuccess', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.crmSSORequestSuccess().type, saga.crmSSORequestSuccess));
    });

    it('watchConnector should takeEvery crmSSORequestFailure', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.crmSSORequestFailure().type, saga.crmSSORequestFailure));
    });

    it('watchConnector should takeEvery crmSSORequestFailure', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.windowUnload().type, saga.unloadLocalStorageManagerSids));
    });

    it('watchConnector should takeEvery sendRequest', () => {
        assert.deepEqual(generatorWatchConnector.next().value, takeEvery(mockActions.sendRequest().type, saga.onSendRequest));
    });

    it('watchConnector should finished', () => {
        assert.deepEqual(generatorWatchConnector.next().done, true);
    });
});

describe('connector saga unloadLocalStorageManagerSids', () => {
    const generator = saga.unloadLocalStorageManagerSids();

    it('unloadLocalStorageManagerSids should select isLocalStorageSupport', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.isLocalStorageSupport));
    });

    it('unloadLocalStorageManagerSids should select getSessionData', () => {
        assert.deepEqual(generator.next(isLocalStorageSupport).value, select(selectorsConnector.getSessionData));
    });

    it('unloadLocalStorageManagerSids should call getRemovedSid', () => {
        const sids = sessionData.sids ? { ...sessionData.sids } : {};
        assert.deepEqual(generator.next(sessionData).value, call(getRemovedSid, sids, sessionData.sid));
    });

    it('unloadLocalStorageManagerSids should put setItemSidsLS', () => {
        const removedSids = [];
        assert.deepEqual(generator.next(removedSids).value, put(mockActions.setItemSidsLS(removedSids)));
    });

    it('unloadLocalStorageManagerSids should be finished', () => {
        assert.deepEqual(generator.next().done, true);
    });

    it('unloadLocalStorageManagerSids sids is full', () => {
        const generatorFullSids = saga.unloadLocalStorageManagerSids();
        generatorFullSids.next();
        generatorFullSids.next(isLocalStorageSupport);
        const data = {
            sids: [],
        };
        generatorFullSids.next(data);
        generatorFullSids.next([]);
        assert.isTrue(generatorFullSids.next().done);
    });
});

describe('connector saga logout', () => {
    const generatorConnKeepAlive = saga.connKeepAlive();
    let channel = intervalEventChannel(keepAliveParams.keepAliveTimeOut);
    generatorConnKeepAlive.next();
    // generatorConnKeepAlive.next(keepAliveParams);
    generatorConnKeepAlive.next(keepAliveParams);
    generatorConnKeepAlive.next(channel);

    const action = mockActions.logout({
        payload: 'test',
    });
    const generator = saga.logout(action);

    it('logout should select getSessionData', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.getSessionData));
    });

    it('logout should call getRequestString', () => {
        const type = mockActions.logout().type;
        assert.deepEqual(generator.next(sessionData).value, call(RequestSender.getRequestString, type));
    });

    it('logout should call sendRequest', () => {
        const requestString = RequestSender.getRequestString(mockActions.logout().type);
        assert.deepEqual(generator.next(requestString).value, call(sendRequest, requestString));
    });

    it('logout should call closeWebSocket', () => {
        assert.deepEqual(generator.next().value, call(closeWebSocket));
    });

    it('logout should call getRemovedSid', () => {
        assert.deepEqual(generator.next().value, call(saga.getRemovedSid, sessionData.sids, sessionData.sid));
    });

    it('logout should put setItemSidsLS', () => {
        const removedSid = saga.getRemovedSid(sessionData.sids, sessionData.sid);
        assert.deepEqual(generator.next(removedSid).value, put(mockActions.setItemSidsLS(removedSid)));
    });

    it('logout should put setItemSessionTid', () => {
        const closedSession = {
            lang: sessionData.lang,
            th: sessionData.th,
        };
        assert.deepEqual(generator.next(sessionData).value, put(mockActions.setItemSessionTid({
            itemName: `_session_${sessionData.tid}`,
            itemObject: closedSession,
        })));
    });

    it('logout should call assignLocation', () => {
        const logoutParams = action.payload ? `login.html?${action.payload}` : `login.html?v=1&op=logout&lang=${sessionData.lang}`;
        assert.deepEqual(generator.next(sessionData).value, call(assignLocation, logoutParams));
    });

    it('logout should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });

    it('logout with out payload', () => {
        const generator = saga.logout({
            type: 'test',
        });
        generator.next();
        generator.next(sessionData);
        generator.next();
        generator.next();
        generator.next();
        generator.next();
        generator.next();
        assert.deepEqual(generator.next().value, call(assignLocation, `login.html?v=1&op=logout&lang=${sessionData.lang}`));
        assert.isTrue(generator.next().done);
    });

    after(() => {
        channel.close();
    });
});

describe('connector saga init', () => {
    const generatorInit = saga.init();

    it('init should return select getSessionData', () => {
        assert.deepEqual(generatorInit.next().value, select(selectorsConnector.getSessionData));
    });

    it('init should return select isLocalStorageSupport', () => {
        assert.deepEqual(generatorInit.next(sessionData).value, select(selectorsConnector.isLocalStorageSupport));
    });

    it('init should return select keepAliveParams', () => {
        assert.deepEqual(generatorInit.next(true).value, select(selectorsConnector.getKeepAliveParams));
    });

    it('init no sessionData.sid should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next({ lang: 'ru' });
        generator.next();
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('NOSESSION'))));
    });

    it('init no sessionData.su should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next({ sid: {}, lang: 'ru' });
        generator.next();
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('NOSESSION'))));
    });

    it('init no sessionData.sp should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next({ sid: {}, su: {}, lang: 'ru' });
        generator.next();
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('NOSESSION'))));
    });

    it('init no sessionData.sid should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next({ sid: null, lang: 'ru' });
        generator.next();
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('NOSESSION'))));
    });

    it('init no isLocalStorageSupport should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next(sessionData);
        generator.next(false);
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('LSNOSUPPORT'))));
    });

    it('init sid duplicated should return put logout', () => {
        const generator = saga.init();
        generator.next();
        generator.next({
            ...sessionData,
            sids: {
                [sessionData.sid]: 111111111,
            },
        });
        generator.next(true);
        assert.deepEqual(generator.next().value, put(mockActions.logout(getLogoutCode('DUPLICATE'))));
    });

    it('init should return call getCurrentTime', () => {
        assert.deepEqual(generatorInit.next(keepAliveParams).value, call(getCurrentTime));
    });

    it('init should return put setItemSidsLS', () => {
        const time = getCurrentTime();
        const act = generatorInit.next(time).value;
        act.PUT.action.payload[sessionData.sid] = null;
        const exp = put(mockActions.setItemSidsLS({
            ...sessionData.sids,
            [sessionData.sid]: null,
        }));
        assert.deepEqual(act, exp);
    });

    it('init should return put setItemSidsStore', () => {
        const act = generatorInit.next().value;
        act.PUT.action.payload[sessionData.sid] = null;
        const exp = put(mockActions.setItemSidsStore({
            ...sessionData.sids,
            [sessionData.sid]: null,
        }));
        assert.deepEqual(act, exp);
    });

    it('init should return put connect', () => {
        assert.deepEqual(generatorInit.next().value, put(mockActions.connect()));
    });

    it('init if keepAliveParams.isKeepAlive true should return call connKeepAlive', () => {
        assert.deepEqual(generatorInit.next().value, call(saga.connKeepAlive));
    });

    it('init should finished', () => {
        assert.deepEqual(generatorInit.next().done, true);
    });

    it('init if keepAliveParams.isKeepAlive false finished', () => {
        const generator = saga.init();
        generator.next();
        generator.next(sessionData);
        generator.next(true);
        generator.next({ isKeepAlive: false });
        generator.next();
        generator.next();
        generator.next();
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga connect', () => {
    const generatorConnect = saga.connect();
    it('connect should return call getConnectionStatusLocalStorage', () => {
        assert.deepEqual(generatorConnect.next().value, call(saga.getConnectionStatusLocalStorage));
    });

    it('connect should return select getSessionId', () => {
        assert.deepEqual(generatorConnect.next(true).value, select(selectorsConnector.getSessionId));
    });

    it('connect should return select getCustomMarker', () => {
        assert.deepEqual(generatorConnect.next(sessionData.sid).value, select(selectorsConnector.getCustomMarker));
    });

    it('connect should call initRequestSenderSessionData', () => {
        assert.deepEqual(
            generatorConnect.next(customMarker).value,
            call(RequestSender.initRequestSenderSessionData, sessionData.sid, customMarker)
        );
    });

    it('connect should return put getCustomMarker', () => {
        assert.deepEqual(generatorConnect.next().value, put(mockActions.connectWs()));
    });

    it('connect should finished', () => {
        assert.deepEqual(generatorConnect.next().done, true);
    });

    it('connect if connectionStatus false should return put reconnectRequest and false', () => {
        const generator = saga.connect();
        generator.next();
        assert.deepEqual(generator.next(false).value, put(mockActions.reconnectRequest()));
        assert.isFalse(generator.next().value);
    });

    it('connect reconnectionState = false case', () => {
        const generatorReconnectRequest = saga.reconnectRequest();
        generatorReconnectRequest.next();

        const generatorConnect = saga.connect();
        generatorConnect.next();
        generatorConnect.next();
        generatorConnect.next();
        generatorConnect.next();
        assert.isTrue(generatorConnect.next().done);
    });
});

describe('connector saga keepAliveRequest', () => {
    const action = mockActions.keepAliveRequest();
    const generator = saga.keepAliveRequest(action);

    it('keepAliveRequest should return select getKeepAliveParams', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.getKeepAliveParams));
    });

    it('keepAliveRequest should return select getSessionData', () => {
        assert.deepEqual(generator.next(keepAliveParams).value, select(selectorsConnector.getSessionData));
    });

    it('keepAliveRequest should return call getTime', () => {
        assert.deepEqual(generator.next(sessionData).value, call(getCurrentTime));
    });

    it('keepAliveRequest should return call requestGET', () => {
        const time = getCurrentTime();
        assert.deepEqual(generator.next(time).value, call(requestGET, action.type,
            `${keepAliveParams.keepAliveService}?sid=${sessionData.sid}&t=${time}`,
            keepAliveParams.keepAliveTimeOut));
    });

    it('keepAliveRequest should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga -> keepAliveRequestSuccess', () => {
    const generator = saga.keepAliveRequestSuccess();

    it('keepAliveRequest should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga keepAliveRequestFailure', () => {
    const action = mockActions.keepAliveRequestFailure({
        status: 200,
    });
    const generatorKeepAlive = saga.keepAliveRequestFailure(action);

    it('keepAliveRequestFailure should return select getSessionData', () => {
        assert.deepEqual(generatorKeepAlive.next().value, select(selectorsConnector.getSessionData));
    });

    it('keepAliveRequestFailure should return put setConnectionStateStore', () => {
        assert.deepEqual(generatorKeepAlive.next(sessionData).value, put(mockActions.setConnectionStateStore(false)));
    });

    it('keepAliveRequestFailure should return put reconnectRequest', () => {
        assert.deepEqual(generatorKeepAlive.next().value, put(mockActions.reconnectRequest()));
    });

    it('keepAliveRequestFailure should finished', () => {
        assert.deepEqual(generatorKeepAlive.next().done, true);
    });

    it('keepAliveRequestFailure if status 502 should return put logout', () => {
        const action = mockActions.keepAliveRequestFailure({
            status: 502,
        });
        const generator = saga.keepAliveRequestFailure(action);
        generator.next();
        assert.deepEqual(generator.next(sessionData).value, put(mockActions.logout(getLogoutCode(action.payload.status))));
    });

    it('keepAliveRequestFailure if status 400 should return put logout', () => {
        const action = mockActions.keepAliveRequestFailure({
            status: 400,
        });
        const generator = saga.keepAliveRequestFailure(action);
        generator.next();
        assert.deepEqual(generator.next(sessionData).value, put(mockActions.logout(getLogoutCode(action.payload.status))));
    });

    it('keepAliveRequestFailure if status 404 should return put logout', () => {
        const action = mockActions.keepAliveRequestFailure({
            status: 404,
        });
        const generator = saga.keepAliveRequestFailure(action);
        generator.next();
        assert.deepEqual(generator.next(sessionData).value, put(mockActions.logout(getLogoutCode(action.payload.status))));
    });
});

describe('connector saga connectWebSocketError', () => {
    it('connectWebSocketError no error headers', () => {
        const action = mockActions.getUserSettings({});
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next().value, put(mockActions.reconnectRequest()));
        assert.deepEqual(generator.next().done, true);
    });

    it('connectWebSocketError no error headers message', () => {
        const action = mockActions.getUserSettings({
            headers: {
                message: 'test',
            },
        });
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next().value, put(mockActions.connect()));
        assert.deepEqual(generator.next().done, true);
    });

    it('connectWebSocketError error message = not_found', () => {
        const action = mockActions.getUserSettings({
            headers: {
                message: 'not_found',
            },
        });
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next().value, put(mockActions.reconnectRequest()));
        assert.deepEqual(generator.next().done, true);
    });

    it('connectWebSocketError error message = access_refused', () => {
        const lang = 'ru';
        const action = mockActions.getUserSettings({
            headers: {
                message: 'access_refused',
            },
        });
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next(lang).value, put(mockActions.logout(`err=session_expired&lang=${lang}`)));
        assert.deepEqual(generator.next().done, true);
    });

    it('connectWebSocketError error message = Bad CONNECT', () => {
        const lang = 'ru';
        const action = mockActions.getUserSettings({
            headers: {
                message: 'Bad CONNECT',
            },
        });
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next(lang).value, put(mockActions.logout(`err=session_expired&lang=${lang}`)));
        assert.deepEqual(generator.next().done, true);
    });

    it('connectWebSocketError error message = Server cancelled subscription', () => {
        const lang = 'ru';
        const action = mockActions.getUserSettings({
            headers: {
                message: 'Server cancelled subscription',
            },
        });
        const generator = saga.connectWebSocketError(action);
        assert.deepEqual(generator.next().value, call(closeWebSocket));
        assert.deepEqual(generator.next().value, select(selectorsConnector.getLanguage));
        assert.deepEqual(generator.next(lang).value, put(mockActions.logout(`err=session_canceled&lang=${lang}`)));
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga reconnectRequest', () => {
    const action = mockActions.reconnectRequest();
    const generatorReconnectRequest = saga.reconnectRequest(action);
    const reconnectParams = mockSelectors.getReconnectParams(state);

    it('reconnectRequest should return call closeWebSocket', () => {
        assert.deepEqual(generatorReconnectRequest.next().value, call(closeWebSocket));
    });

    it('reconnectRequest should return select getReconnectParams', () => {
        assert.deepEqual(generatorReconnectRequest.next().value, select(selectorsConnector.getReconnectParams));
    });

    it('reconnectRequest should return select getSessionData', () => {
        assert.deepEqual(generatorReconnectRequest.next(reconnectParams).value, select(selectorsConnector.getSessionData));
    });

    it('reconnectRequest should return call requestPOST', () => {
        assert.deepEqual(
            generatorReconnectRequest.next(sessionData).value,
            call(requestPOST, action.type, reconnectParams.loginService, `su=${sessionData.su}&sp=${sessionData.sp}`, reconnectParams.keepAliveTimeOut)
        );
    });

    it('reconnectRequest should finished', () => {
        assert.deepEqual(generatorReconnectRequest.next().done, true);
    });
});

describe('connector saga reconnectRequestSuccess', () => {
    delete sessionData.sids;
    const mockData = {
        sid: 'test',
        stompUser: 'testSU',
        stompPassword: 'testSP',
    };

    const newSession = {
        ...sessionData,
        sid: mockData.sid,
        su: mockData.stompUser,
        sp: mockData.stompPassword,
    };

    const action = mockActions.reconnectRequestSuccess({
        responseText: JSON.stringify({ ...mockData }),
    });

    const generatorReconnectRequestSuccess = saga.reconnectRequestSuccess(action);

    it('reconnectRequestSuccess should return select getSessionData', () => {
        assert.deepEqual(generatorReconnectRequestSuccess.next().value, select(selectorsConnector.getSessionData));
    });

    it('reconnectRequestSuccess should return put setItemSessionTid', () => {
        assert.deepEqual(generatorReconnectRequestSuccess.next(sessionData).value, put(mockActions.setItemSessionTid({
            itemName: `_session_${sessionData.tid}`,
            itemObject: newSession,
        })));
    });

    it('reconnectRequestSuccess should return put setSessionData', () => {
        assert.deepEqual(generatorReconnectRequestSuccess.next().value, put(mockActions.setSessionData(newSession)));
    });

    it('reconnectRequestSuccess should return put connect', () => {
        assert.deepEqual(generatorReconnectRequestSuccess.next().value, put(mockActions.connect()));
    });

    it('reconnectRequestSuccess should finished', () => {
        assert.deepEqual(generatorReconnectRequestSuccess.next().done, true);
    });

    it('reconnectRequestSuccess should return put logout', () => {
        const res = { code: 404 };
        action.payload = {
            responseText: JSON.stringify(res),
        };
        const generator = saga.reconnectRequestSuccess(action);
        generator.next().value;
        assert.deepEqual(generator.next().value, put(mockActions.logout(`REGENERATE_ERROR=${res.code}`)));
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga reconnectRequestFailure', () => {
    const generatorReconnectRequestFailure = saga.reconnectRequestFailure();

    it('reconnectRequestFailure should return put setConnectionStateStore', () => {
        assert.deepEqual(generatorReconnectRequestFailure.next().value, put(mockActions.setConnectionStateStore(false)));
    });

    it('reconnectRequestFailure should return call setConnectionStatusLocalStorage', () => {
        assert.deepEqual(generatorReconnectRequestFailure.next().value, call(saga.setConnectionStatusLocalStorage, false));
    });

    it('reconnectRequestFailure should return put reconnectRequest', () => {
        assert.deepEqual(generatorReconnectRequestFailure.next().value, put(mockActions.reconnectRequest()));
    });

    it('reconnectRequestFailure should finished', () => {
        assert.deepEqual(generatorReconnectRequestFailure.next().done, true);
    });
});

describe('connector saga getConnectionStatusLocalStorage', () => {
    const generator = saga.getConnectionStatusLocalStorage();
    const isConnectionAlive = true;

    it('getConnectionStatusLocalStorage should return select isConnectionAlive', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.isConnectionAlive));
    });

    it('getConnectionStatusLocalStorage should finished', () => {
        assert.deepEqual(generator.next(isConnectionAlive).value, true);
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga setConnectionStatusLocalStorage', () => {
    const isConnectionAlive = true;
    const generator = saga.setConnectionStatusLocalStorage(isConnectionAlive);

    it('setConnectionStatusLocalStorage should return select getSessionData', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.getSessionData));
    });

    it('setConnectionStatusLocalStorage should return put setItemSessionTid', () => {
        assert.deepEqual(generator.next(sessionData).value, put(mockActions.setItemSessionTid({
            itemName: `_session_${sessionData.tid}`,
            itemObject: sessionData,
        })));
    });

    it('setConnectionStatusLocalStorage should return put setSessionData', () => {
        assert.deepEqual(generator.next().value, put(mockActions.setSessionData(sessionData)));
    });

    it('setConnectionStatusLocalStorage should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga connKeepAlive', () => {
    const generatorConnKeepAlive = saga.connKeepAlive();

    it('connKeepAlive should return select getKeepAliveParams', () => {
        assert.deepEqual(generatorConnKeepAlive.next().value, select(selectorsConnector.getKeepAliveParams));
    });

    it('connKeepAlive should return call channelKeepAlive', () => {
        assert.deepEqual(generatorConnKeepAlive.next(keepAliveParams).value, call(intervalEventChannel, keepAliveParams.keepAliveTimeOut));
    });

    it('connKeepAlive should finished', () => {
        assert.deepEqual(generatorConnKeepAlive.next().done, true);
    });

    it('connKeepAlive should return put channelKeepAlive', async () => {
        const generator = saga.connKeepAlive();
        generator.next();
        generator.next(keepAliveParams);
        const channel = intervalEventChannel(500);
        const takenAction = new Promise(resolve => {
            channel.take(resolve);
        });
        const action = await takenAction;
        assert.deepEqual(generator.next(channel).value, take(channel));
        assert.deepEqual(generator.next(action).value, put(action));
        channel.close();
    });
});

describe('connector saga changeLanguage', () => {
    const lang = 'en';
    const action = mockActions.changeLanguage(lang);
    const generatorChangeLanguage = saga.changeLanguage(action);

    it('changeLanguage should return select getSessionData', () => {
        assert.deepEqual(generatorChangeLanguage.next().value, select(selectorsConnector.getSessionData));
    });

    it('changeLanguage should return put setItemSessionTid', () => {
        sessionData.lang = lang;
        assert.deepEqual(generatorChangeLanguage.next(sessionData).value, put(mockActions.setItemSessionTid({
            itemName: `_session_${sessionData.tid}`,
            itemObject: sessionData,
        })));
    });

    it('changeLanguage should put setSessionData', () => {
        assert.deepEqual(generatorChangeLanguage.next().value, put(mockActions.setSessionData(sessionData)));
    });

    it('changeLanguage should finished', () => {
        assert.deepEqual(generatorChangeLanguage.next().done, true);
    });
});

describe('connector saga crmSSORequest', () => {
    const action = mockActions.crmSSORequest({
        token: 'test',
    });
    const generator = saga.crmSSORequest(action);

    it('keepAliveRequest should return select getKeepAliveParams', () => {
        assert.deepEqual(generator.next().value, select(selectorsConnector.getKeepAliveParams));
    });

    it('keepAliveRequest should return select getCRMApiUrl', () => {
        assert.deepEqual(generator.next(keepAliveParams).value, select(selectorsConnector.getCRMApiUrl));
    });

    it('keepAliveRequest should return call requestPOST', () => {
        const crmApiUrl = mockSelectors.getCRMApiUrl(state);
        assert.deepEqual(generator.next(crmApiUrl).value, call(requestPOST, action.type, `${crmApiUrl}/account/logonWithToken`, `token=${action.payload.token}`, keepAliveParams.keepAliveTimeOut));
    });

    it('keepAliveRequest should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga crmSSORequestSuccess', () => {
    const action = mockActions.crmSSORequestSuccess();
    const generator = saga.crmSSORequestSuccess(action);

    it('crmSSORequestSuccess should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga connectWebSocketSuccess', () => {
    const generator = saga.connectWebSocketSuccess();

    it('connectWebSocketSuccess should return call sendRequest', () => {
        assert.deepEqual(generator.next().value, call(sendRequest, RequestSender.getRequestString(mockActions.getTime().type.toUpperCase())));
    });

    it('connectWebSocketSuccess should finished', () => {
        assert.deepEqual(generator.next().done, true);
    });
});

describe('connector saga connectWebSocketError reconnectionState true', () => {
    it('connectWebSocketError should return false and finished', () => {
        const reconnectRequestGen = saga.reconnectRequest();
        reconnectRequestGen.next();
        const generator = saga.connectWebSocketError();
        assert.deepEqual(generator.next(), { value: false, done: true });
    });
});

describe('connector saga getRemovedSid', () => {
    it('getRemovedSid should return newSids', () => {
        const mockSession = { '11111111-1111-1111-1111-111111111111': 999999999999999 };
        const exp = { ...mockSession };
        const sids = {
            '4b2f1d34-5bbb-4209-88fe-348f4bf4b77f': 0,
            ...mockSession,
        };
        assert.deepEqual(saga.getRemovedSid(sids, sessionData.sid), exp);
    });

    it('getRemovedSid should return {}', () => {
        const mockSession = { '11111111-1111-1111-1111-111111111111': 1 };
        const exp = {};
        const sids = {
            '4b2f1d34-5bbb-4209-88fe-348f4bf4b77f': 0,
            ...mockSession,
        };
        assert.deepEqual(saga.getRemovedSid(sids, sessionData.sid), exp);
    });
});

describe('connector onSendRequest', () => {
    const payload = {
        command: 'test_command',
        params: { testParam: 'test_param' },
    };
    const action = { type: 'test', payload: payload };
    const message = 'test_message';

    const generator = saga.onSendRequest(action);

    it('onSendRequest should call getRequestString', () => {
        assert.deepEqual(generator.next().value, call(RequestSender.getRequestString, action.payload.command, action.payload.params));
    });

    it('onSendRequest should call sendRequest', () => {
        assert.deepEqual(generator.next(message).value, call(sendRequest, message));
    });

    it('onSendRequest should be finished', () => {
        assert.isTrue(generator.next().done);
    });
});

describe('connector saga crmSSORequestFailure', () => {
    it('crmSSORequestFailure should call console.warn', () => {
        const sandbox = sinon.createSandbox();

        const warn = sandbox.spy(global.console, 'warn');
        const mockAction = {
            payload: {
                status: 'test',
            },
        };

        saga.crmSSORequestFailure(mockAction);
        const exp = `Warning! Request wasn't sent while SSO status=${mockAction.payload.status}`;
        sinon.assert.calledWith(warn, exp);

        sandbox.restore();
    });
});

describe('connector saga assignLocation', () => {
    let sandbox;
    before(() => {
        sandbox = sinon.createSandbox();
    });
    it('assignLocation should call window.location.assign', () => {
        const assign = sandbox.stub(window.location, 'assign');
        const params = 'test';
        saga.assignLocation(params);
        sinon.assert.calledWith(assign, params);
    });

    it('assignLocation no call window.location.assign', () => {
        const assign = sandbox.spy(window.location, 'assign');
        saga.assignLocation();
        sinon.assert.notCalled(assign);
    });

    afterEach(() => {
        sandbox.restore();
    });
});

