import * as restApi from '../restAPI';
import * as selectors from '../../../mockData/mockSelectors';
import { call, take, put, apply } from 'redux-saga/effects';
import { initXMLHttp } from '../restAPI';

const state = store.getState();
const action = 'test_request';
const keepAliveParams = selectors.getKeepAliveParams(state);
const addressGET = `http://httpbin.org/get`;
const addressPOST = `http://httpbin.org/post`;
const timeout = keepAliveParams.keepAliveTimeOut;
const params = 'test';
const xmlHttp = {
    open: sinon.spy(),
    abort: sinon.spy(),
    timeout: null,
    readyState: null,
    onreadystatechange: null,
    setRequestHeader: sinon.spy(),
};

xmlHttp.send = params => {
    xmlHttp.readyState = 4;
    xmlHttp.onreadystatechange();
};

const initMockXMLHttp = () => {
    const type = 'GET';
    const address = 'test';
    const generator = restApi.initXMLHttp(type, address, timeout);
    generator.next();
    generator.next(xmlHttp);
    generator.next();
};

describe('restApi', () => {
    describe('restApi requestGET', () => {
        const generatorRestApi = restApi.requestGET(action, addressGET, timeout);
        const channel = {
            take: () => {},
            flush: () => {},
            close: () => {},
        };

        it('requestGET should return call initXMLHttp', () => {
            assert.deepEqual(generatorRestApi.next().value, call(initXMLHttp, `GET`, addressGET, timeout));
        });

        it('requestGET should return call request', () => {
            assert.deepEqual(generatorRestApi.next().value, call(restApi.request, action, null));
        });

        it('requestGET should return take channel', () => {
            assert.deepEqual(generatorRestApi.next(channel).value, take(channel));
        });

        it('requestGET should return put response', async () => {
            const responce = 'test';
            assert.deepEqual(generatorRestApi.next(responce).value, put(responce));
        });

        it('requestGET should finished', () => {
            assert.deepEqual(generatorRestApi.next().done, true);
        });

        it('requestGET should return false if channel isError', () => {
            const generator = restApi.requestGET(action, null, timeout);
            generator.next();
            generator.next();
            generator.next(channel);
            assert.deepEqual(generator.next({ isError: true }).value, false);
        });
    });

    describe('restApi requestPOST', () => {
        const generatorRestApi = restApi.requestPOST(action, addressPOST, params, timeout);
        const channel = {
            take: () => {},
            flush: () => {},
            close: () => {},
        };

        it('requestPOST should return call initXMLHttp', () => {
            assert.deepEqual(generatorRestApi.next().value, call(initXMLHttp, `POST`, addressPOST, timeout));
        });

        it('requestPOST should return call request', () => {
            assert.deepEqual(generatorRestApi.next().value, call(restApi.request, action, params));
        });

        it('requestPOST should return take channel', () => {
            assert.deepEqual(generatorRestApi.next(channel).value, take(channel));
        });

        it('requestPOST should return put response', async () => {
            const responce = 'test';
            assert.deepEqual(generatorRestApi.next(responce).value, put(responce));
        });

        it('requestPOST should finished', () => {
            assert.deepEqual(generatorRestApi.next().done, true);
        });

        it('requestPOST should return false if channel isError', async () => {
            const generator = restApi.requestPOST(action, null, params, timeout);
            generator.next();
            generator.next();
            generator.next(channel);

            assert.deepEqual(generator.next({ isError: true }).value, false);
        });
    });

    describe('restApi onResponse', () => {
        let messageEmit = null;
        const action = 'test';
        const emitter = message => {
            messageEmit = message;
        };
        const response = {
            readyState: 4,
            status: 200,
            responseText: 'test',
        };

        let sandbox;
        before(() => {
            sandbox = sinon.createSandbox();
        });

        it(`onResponse should return ${action}_success`, () => {
            const exp = {
                type: `${action}_success`,
                payload: { ...response },
            };
            restApi.onResponse(emitter, response, action);
            assert.deepEqual(messageEmit, exp);
        });

        it(`onResponse should return ${action}_failure`, () => {
            response.status = 100;
            const exp = {
                type: `${action}_failure`,
                payload: { ...response },
            };
            restApi.onResponse(emitter, response, action);
            assert.deepEqual(messageEmit, exp);
        });

        it(`onResponse should return false if no emitter`, () => {
            const error = sandbox.spy(global.console, 'error');
            restApi.onResponse(null, response, action);
            sinon.assert.called(error);
        });

        it(`onResponse should return false if no response`, () => {
            const error = sandbox.spy(global.console, 'error');
            restApi.onResponse(null, response, action);
            sinon.assert.called(error);
        });

        it(`onResponse should catch error`, () => {
            const error = sandbox.spy(global.console, 'error');
            restApi.onResponse(null, { status: 200 }, 'test');
            sinon.assert.called(error);
        });

        it(`onResponse should return false if no action`, () => {
            const error = sandbox.spy(global.console, 'error');
            assert.isFalse(restApi.onResponse(null, { status: 200 }));
        });

        afterEach(() => {
            sandbox.restore();
        });
    });

    describe('restApi onError', () => {
        let messageEmit = null;
        const emitter = message => {
            messageEmit = message;
        };
        let sandbox;
        before(() => {
            sandbox = sinon.createSandbox();
        });

        it(`onError should return isError true`, () => {
            restApi.onError(emitter);
            assert.deepEqual(messageEmit, { isError: true });
        });

        it('request should catch error', () => {
            const error = sandbox.spy(global.console, 'error');
            restApi.onError();
            sinon.assert.called(error);
        });

        afterEach(() => {
            sandbox.restore();
        });
    });

    describe('restApi initXMLHttp', () => {
        const type = 'GET';
        const address = 'test';
        const generator = restApi.initXMLHttp(type, address, timeout);

        it('initXMLHttp should return call createXMLHttp', () => {
            assert.deepEqual(generator.next().value, call(restApi.createXMLHttp, type, address, timeout));
        });

        it('initXMLHttp should return call createXMLHttp', () => {
            assert.deepEqual(generator.next().value, call(restApi.createXMLHttp, type, address, timeout));
        });

        it('initStompClient should finish', () => {
            assert.isTrue(generator.next().done);
        });
    });

    describe('restApi createXMLHttp', () => {
        const type = 'GET';
        const address = 'test';
        const generator = restApi.createXMLHttp(type, address, timeout);

        it('createXMLHttp should return call getXMLHttp', () => {
            assert.deepEqual(generator.next().value, call(restApi.getXMLHttp));
        });

        it('initStompClient should return apply xmlHttp.open', () => {
            assert.deepEqual(generator.next(xmlHttp).value, apply(xmlHttp, xmlHttp.open, [type, address]));
        });

        it('initStompClient should return apply xmlHttp.setRequestHeader', () => {
            assert.deepEqual(generator.next().value, apply(xmlHttp, xmlHttp.setRequestHeader, ['Content-type', 'application/x-www-form-urlencoded']));
        });

        it('initStompClient should finish', () => {
            assert.isTrue(generator.next().done);
        });

        it('initStompClient check params', () => {
            assert.deepEqual(xmlHttp.timeout, timeout);
        });
    });

    describe('restApi abortXMLHttp', () => {
        before(() => {
            restApi.abortXMLHttp();
        });

        it(`abortXMLHttp should abort xmlHttp`, () => {
            initMockXMLHttp();
            restApi.abortXMLHttp();
            sinon.assert.called(xmlHttp.abort);
        });
    });

    describe('restApi getXMLHttp', () => {
        it(`abortXMLHttp should return new XMLHttp`, () => {
            const act = typeof restApi.getXMLHttp();
            assert.equal(act, 'object');
        });
    });

    describe('restApi request', () => {
        let sandbox; let channel;
        before(() => {
            sandbox = sinon.createSandbox();
        });

        it('request should call XMLHttp.onreadystatechange', () => {
            initMockXMLHttp();
            sandbox.spy(xmlHttp, 'send');
            const params = 'test';
            channel = restApi.request(action, params);
            sinon.assert.calledWith(xmlHttp.send, params);
        });

        it('request should catch error', () => {
            restApi.abortXMLHttp();
            const error = sandbox.spy(global.console, 'error');
            channel = restApi.request(action, params);
            sinon.assert.called(error);
        });

        afterEach(() => {
            channel.close();
            sandbox.restore();
        });
    });
});
