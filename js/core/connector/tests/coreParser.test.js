import Parser from '../parser/coreParser';
import * as mockActions from '../../../mockData/mockActions';
import * as connectorSelectors from '../../connector/selectors';
import { put, select } from 'redux-saga/effects';
import { getConnectorParser, getConnectorObjectKeys } from '../../../mockData/mockParserKeys';

const connectorObjectKeys = getConnectorObjectKeys();

describe('check keys', () => {
    const mockParser = getConnectorParser();
    it(`check upper level`, () => {
        assert.deepEqual(Object.keys(Parser), Object.keys(mockParser));
    });

    it(`check event level`, () => {
        assert.deepEqual(Object.keys(Parser.event), Object.keys(mockParser.event));
    });

    it(`check response level`, () => {
        assert.deepEqual(Object.keys(Parser.response), Object.keys(mockParser.response));
    });

    it(`check update level`, () => {
        assert.deepEqual(Object.keys(Parser.update), Object.keys(mockParser.update));
    });
});

describe('coreParser setCrmSingleSignOn method', () => {
    const data = {
        token: 'test',
        currentAccountCrmId: 'test',
    };
    const action = connectorObjectKeys.CRM_SINGLE_SIGN_ON().type;
    const generatorSrmSSO = Parser.update[action](data);

    it('setCrmSingleSignOn should return the crmSSORequest action', () => {
        assert.deepEqual(generatorSrmSSO.next().value, put(mockActions.crmSSORequest({
            token: data.token,
            currentAccountCrmId: data.currentAccountCrmId,
        })));
    });

    it('setCrmSingleSignOn should be finished', () => {
        assert.deepEqual(generatorSrmSSO.next().done, true);
    });

    it('setCrmSingleSignOn should return false', () => {
        const generator = Parser.update[action]();
        assert.deepEqual(generator.next().value, false);
    });

    it('setCrmSingleSignOn should return false', () => {
        const generator = Parser.update[action]({ token: 'test' });
        assert.deepEqual(generator.next().value, false);
    });

    it('setCrmSingleSignOn should return false', () => {
        const generator = Parser.update[action]({ currentAccountCrmId: 'test' });
        assert.deepEqual(generator.next().value, false);
    });
});

describe('coreParser getTradingStatus method', () => {
    const action = connectorObjectKeys.GET_TRADING_STATUS().type;
    const generatorTradingStatus = Parser.response[action]({ tradingDisabled: {} });

    it(`generatorTradingStatus should return selector getLanguage`, () => {
        assert.deepEqual(generatorTradingStatus.next().value, select(connectorSelectors.getLanguage));
    });

    it('generatorTradingStatus should return the logout action', () => {
        const lang = global.store.getState().sessionData.lang;
        assert.deepEqual(generatorTradingStatus.next(lang).value, put(mockActions.logout(`err=trading_disabled&lang=${lang}`)));
    });

    it('generatorTradingStatus should be finished', () => {
        assert.deepEqual(generatorTradingStatus.next().done, true);
    });

    it('generatorTradingStatus should return false', () => {
        const generator = Parser.response[action]({});
        assert.deepEqual(generator.next().value, false);
    });

    it('generatorTradingStatus should return false', () => {
        const generator = Parser.response[action]({ tradingDisabled: false });
        assert.deepEqual(generator.next().value, false);
    });
});

describe('coreParser tradingServerConnectionLost method', () => {
    const action = connectorObjectKeys.TRADING_SERVER_CONNECTION_LOST().type;
    const generatorTradingServerConnectionLost = Parser.event[action]({});

    it(`tradingServerConnectionLost should return put action setConnectionStateStore`, () => {
        assert.deepEqual(generatorTradingServerConnectionLost.next().value, put(mockActions.setConnectionStateStore(false)));
    });

    it('generatorUserAccountOutdated should be finished', () => {
        assert.deepEqual(generatorTradingServerConnectionLost.next().done, true);
    });
});

describe('coreParser logout method', () => {
    const action = connectorObjectKeys.LOGOUT().type;
    const generatorLogout = Parser.response[action]();

    it(`logout should return put action setConnectionStateStore`, () => {
        assert.deepEqual(generatorLogout.next().value, put(mockActions.logout()));
    });
});

