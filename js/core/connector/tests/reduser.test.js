import reducer from '../reducer';
import * as actions from '../../../mockData/mockActions';
import sessionData from '../../../mockData/mockSessionData';

describe('connector Reducer', function () {
    it('default', function () {
        assert.deepEqual(reducer(sessionData, {}), sessionData);
    });

    it('not success action type', function () {
        assert.deepEqual(reducer(sessionData, { type: 'test' }), sessionData);
    });

    it('action type SET_SESSION_DATA_STORE without payload', function () {
        assert.deepEqual(reducer(sessionData, actions.setSessionData()), sessionData);
    });

    it('action type SET_SESSION_DATA_STORE', function () {
        assert.deepEqual(reducer(sessionData, actions.setSessionData(sessionData)), sessionData);
    });

    it('action type SET_CONNECTION_STATE_STORE', function () {
        assert.deepEqual(reducer(sessionData, actions.setConnectionStateStore(false)), {
            ...sessionData,
            isConnectAlive: false,
        });
    });

    it('action type SET_SIDS_STORE', function () {
        const sids = { test: 1, test2: 2 };
        assert.deepEqual(reducer(sessionData, actions.setItemSidsStore(sids)), {
            ...sessionData,
            sids: sids,
        });
    });
});