import * as Parser from '../parser/Parser';
import { call, put } from 'redux-saga/effects';
import { getConnectorParser } from '../../../mockData/mockParserKeys';
import { getUserStatisticServer } from '../../../mockData/mockUserStatistic';
import { errorOpenOptionServer, openedOptionsServer } from '../../../mockData/mockData';
import { convertMessageToBytes, decodeMessage } from '../../utils/decodeMessage';
import * as decodeMessageSource from '../../utils/decodeMessage';
import { decode } from 'msgpack-lite';
import decodeSource from 'msgpack-lite';
import * as mockActions from '../../../mockData/mockActions';

describe('Parser methods', () => {
    let parserCommandKeeper = null;
    let jsonobject = null;
    let restype = null;
    let eventtype = null;
    let rescontent = null;

    before(() => {
        parserCommandKeeper = getConnectorParser();
        parserCommandKeeper.response['get_user_statistics'] = () => {};
        Parser.initParsers(parserCommandKeeper);
        jsonobject = decodeMessage(decode(convertMessageToBytes(getUserStatisticServer().payload.body)));
        restype = jsonobject.type;
        eventtype = jsonobject.type === 'event' ? jsonobject.name : jsonobject.cmd;
        rescontent = jsonobject.content;
    });

    const generatorParseResult = Parser.parseResult(getUserStatisticServer());

    it('combine', () => {
        const mock = {
            update: { test: null },
            response: { test: null },
            event: { test: null },
        };

        const exp = {};

        for (let key in parserCommandKeeper){
            exp[key] = {
                ...parserCommandKeeper[key],
                ...mock[key],
            };
        }

        assert.deepEqual(Parser.combine({
            connectorParser: parserCommandKeeper,
            test: mock,
        }), exp);
    });

    it('parseResult should return call parserCommandKeeper', () => {
        assert.deepEqual(generatorParseResult.next().value, call(parserCommandKeeper[restype][eventtype], rescontent));
    });

    it('parseResult should finished', () => {
        assert.deepEqual(generatorParseResult.next().done, true);
    });

    it('parseResult no input data should return false', () => {
        const generator = Parser.parseResult();
        assert.deepEqual(generator.next().value, false);
    });

    it('parseResult error message should return false', () => {
        const generator = Parser.parseResult({ payload: errorOpenOptionServer });
        assert.deepEqual(generator.next().value, false);
    });

    it('parseResult no valid restype should return false', () => {
        const generator = Parser.parseResult({ payload: openedOptionsServer });
        assert.deepEqual(generator.next().value, false);
    });
});

describe('Parser error case', () => {
    const sandbox = sinon.createSandbox();
    const jsonobject = { code: 0, type: 'response', content: 'content' };
    const action = { payload: { body: 'test' } };
    const generator = Parser.parseResult(action);

    before(() => {
        sandbox.stub(decodeMessageSource, 'decodeMessage').returns(jsonobject);
        sandbox.stub(decodeSource, 'decode').returns({});
    });

    after(() => {
        sandbox.restore();
    });

    it('parseResult should put error action on response', () => {
        assert.deepEqual(generator.next().value, put(mockActions.error(jsonobject)));
    });

    it('parseResult should be done', () => {
       assert.isTrue(generator.next().done);
    });
});
