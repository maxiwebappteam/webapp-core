import * as actions from '../actions';
import * as mockActions from '../../../mockData/mockActions';

describe('connector Actions', function () {
    it('connect', function () {
        assert.deepEqual(actions.connect(), mockActions.connect());
    });

    it('connectWs', function () {
        assert.deepEqual(actions.connectWs(), mockActions.connectWs());
    });

    it('connectWsSuccess', function () {
        assert.deepEqual(actions.connectWsSuccess(), mockActions.connectWsSuccess());
    });

    it('connectWsError', function () {
        assert.deepEqual(actions.connectWsError(), mockActions.connectWsError());
    });

    it('connectWsMessage', function () {
        assert.deepEqual(actions.connectWsMessage(), mockActions.connectWsMessage());
    });

    it('reconnectRequest', function () {
        assert.deepEqual(actions.reconnectRequest(), mockActions.reconnectRequest());
    });

    it('logout', function () {
        assert.deepEqual(actions.logout({}), mockActions.logout({}));
    });

    it('initConnection', function () {
        assert.deepEqual(actions.initConnection(), mockActions.initConnection());
    });

    it('keepAliveRequest', function () {
        assert.deepEqual(actions.keepAliveRequest(), mockActions.keepAliveRequest());
    });

    it('crmSSORequest', function () {
        const token = 'test';
        const currentAccountCrmId = 'test account';
        assert.deepEqual(actions.crmSSORequest({ token, currentAccountCrmId }), mockActions.crmSSORequest({
            token,
            currentAccountCrmId,
        }));
    });

    it('setSessionData', function () {
        assert.deepEqual(actions.setSessionData({}), mockActions.setSessionData({}));
    });

    it('setConnectionStateStore', function () {
        assert.deepEqual(actions.setConnectionStateStore({}), mockActions.setConnectionStateStore({}));
    });

    it('setItemSidsLS', function () {
        assert.deepEqual(actions.setItemSidsLS({}), mockActions.setItemSidsLS({}));
    });

    it('setItemSidsStore', function () {
        assert.deepEqual(actions.setItemSidsStore({}), mockActions.setItemSidsStore({}));
    });

    it('setItemSessionTid', function () {
        const itemName = 'test';
        const itemObject = 'test account';
        assert.deepEqual(actions.setItemSessionTid({ itemName, itemObject }), mockActions.setItemSessionTid({
            itemName,
            itemObject,
        }));
    });

    it('sendRequest', function () {
        const command = 'testCommand';
        const params = {
            testParam: 'testParam',
        };
        assert.deepEqual(actions.sendRequest(command, params), mockActions.sendRequest(command, params));
    });

    it('error', () => {
        const payload = { test: 'test' };
        assert.deepEqual(actions.error(payload), mockActions.error(payload));
    });
});
