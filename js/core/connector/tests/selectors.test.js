import * as selectors from '../selectors';
import * as mockSelectors from '../../../mockData/mockSelectors';

describe('connector Selectors', function () {
    const state = store.getState();

    it('getSockPath', function () {
        assert.deepEqual(selectors.getSockPath(state), mockSelectors.getSockPath(state));
    });

    it('getSockUser', function () {
        assert.deepEqual(selectors.getSockUser(state), mockSelectors.getSockUser(state));
    });

    it('getLanguage', function () {
        assert.deepEqual(selectors.getLanguage(state), mockSelectors.getLanguage(state));
    });

    it('getSessionTimeId', function () {
        assert.deepEqual(selectors.getSessionTimeId(state), mockSelectors.getSessionTimeId(state));
    });

    it('getSockPassword', function () {
        assert.deepEqual(selectors.getSockPassword(state), mockSelectors.getSockPassword(state));
    });

    it('getSessionId', function () {
        assert.deepEqual(selectors.getSessionId(state), mockSelectors.getSessionId(state));
    });

    it('getSessionIds', function () {
        assert.deepEqual(selectors.getSessionIds(state), mockSelectors.getSessionIds(state));
    });

    it('getSessionData', function () {
        assert.deepEqual(selectors.getSessionData(state), mockSelectors.getSessionData(state));
    });

    it('isConnectionAlive', function () {
        assert.deepEqual(selectors.isConnectionAlive(state), mockSelectors.isConnectionAlive(state));
    });

    it('getCRMApiUrl', function () {
        assert.deepEqual(selectors.getCRMApiUrl(state), mockSelectors.getCRMApiUrl(state));
    });

    it('getCustomMarker', function () {
        assert.deepEqual(selectors.getCustomMarker(state), mockSelectors.getCustomMarker(state));
    });

    it('isLocalStorageSupport', function () {
        assert.deepEqual(selectors.isLocalStorageSupport(state), mockSelectors.isLocalStorageSupport(state));
    });

    it('getKeepAliveParams', function () {
        assert.deepEqual(selectors.getKeepAliveParams(state), mockSelectors.getKeepAliveParams(state));
    });

    it('getReconnectParams', function () {
        assert.deepEqual(selectors.getReconnectParams(state), mockSelectors.getReconnectParams(state));
    });
});
