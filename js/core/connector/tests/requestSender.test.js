import * as RequestSender from '../requestSender/RequestSender';
import { getCoreRequestSender } from '../../../mockData/mockRequestSenderData';
import sessionData from '../../../mockData/mockSessionData';

describe('Request sender methods', () => {
    after(() => {
        RequestSender.initRequestSender(null);
        RequestSender.initRequestSenderSessionData(null, null);
    });
    const requestSenderCommandKeeper = getCoreRequestSender();
    const customMarker = 'test';
    const mockSessioId = sessionData.sid;

    it('combineRequest', () => {
        const mock = {
            test1: null,
            test2: null,
            test3: null,
        };

        const exp = {
            ...requestSenderCommandKeeper,
            ...mock,
        };

        assert.deepEqual(RequestSender.combineRequest({
            requestSender: requestSenderCommandKeeper,
            test: mock,
        }), exp);
    });

    it('getRequestString should return false if no init state', () => {
        assert.equal(RequestSender.getRequestString(), false);
    });

    RequestSender.initRequestSender(null);
    RequestSender.initRequestSenderSessionData(mockSessioId, customMarker);

    it('getRequestString should return false if no rootCommandKeeper', () => {
        assert.equal(RequestSender.getRequestString(), false);
    });

    RequestSender.initRequestSender(requestSenderCommandKeeper);
    RequestSender.initRequestSenderSessionData(mockSessioId, null);

    it('getRequestString should return false if no customMarker', () => {
        assert.equal(RequestSender.getRequestString(), false);
    });

    RequestSender.initRequestSender(requestSenderCommandKeeper);
    RequestSender.initRequestSenderSessionData(null, customMarker);

    it('getRequestString should return false if no sessionId', () => {
        assert.equal(RequestSender.getRequestString(), false);
    });

    RequestSender.initRequestSender(requestSenderCommandKeeper);
    RequestSender.initRequestSenderSessionData(mockSessioId, customMarker);

    it('getRequestString should return false if no command in commandKeeper', () => {
        assert.equal(RequestSender.getRequestString('test'), false);
    });

    it('getRequestString should return command string', () => {
        for (let key in requestSenderCommandKeeper) {
            const action = key;
            const exp = requestSenderCommandKeeper[key]();

            assert.deepEqual(RequestSender.getRequestString(action), exp);
        }
    });
});