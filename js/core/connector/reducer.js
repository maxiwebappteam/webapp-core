import constants from '../constants/constants';

const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case constants.SET_SESSION_DATA_STORE:
            return {
                ...state,
                ...action.payload,
            };
        case constants.SET_CONNECTION_STATE_STORE:
            return {
                ...state,
                isConnectAlive: action.payload,
            };
        case constants.SET_SIDS_STORE:
            return {
                ...state,
                sids: { ...action.payload },
            };
        default: {
            return state;
        }
    }
};
