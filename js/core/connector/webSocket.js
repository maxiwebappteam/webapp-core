import { call, put, select, take, apply } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import * as selectorsConnector from './selectors';
import * as actions from './actions';
import SockJS from 'sockjs-client';
import { Stomp } from '@stomp/stompjs';

let myWebSocket;
export let stompClient;
export let channel;

export function closeWebSocket() {
    if (!channel) {
        return false;
    }

    channel.close();
}

export function onConnect(emitter, sessionId) {
    if (!emitter) {
        return false;
    }
    emitter(actions.connectWsSuccess());
    try {
        stompClient.subscribe(`/amq/queue/session.${sessionId}`, message => {
            emitter(actions.connectWsMessage(message));
        });
    } catch (e) {
        console.error(e);
    }
}

export function onError(emitter, msg) {
    if (!emitter) {
        return false;
    }
    emitter(actions.connectWsError(msg));
}

export function getNewSockJS(socketPath) {
    return new SockJS(socketPath);
}

export function* initStompClient(socketPath) {
    myWebSocket = yield call(getNewSockJS, socketPath);
    stompClient = yield apply(Stomp, Stomp.over, [myWebSocket]);
    stompClient.heartbeat.outgoing = 2000;
    stompClient.heartbeat.incoming = 2000;
    stompClient.debug = () => {};
}

export function closeStompClient() {
    if (!stompClient || !myWebSocket) {
        return false;
    }

    stompClient.disconnect();
    myWebSocket.close();
    stompClient = null;
    myWebSocket = null;
}

export function connectStompClient(socketUser, socketPassword, sessionId) {
    return eventChannel(emitter => {
        try {
            stompClient.connect(
                socketUser,
                socketPassword,
                () => {
                    onConnect(emitter, sessionId);
                },
                error => {
                    onError(emitter, error);
                },
                error => {
                    onError(emitter, error);
                },
                'trading'
            );
        } catch (error) {
            console.error(`initWebSocket ${error}`);
        }

        return () => {
            closeStompClient();
        };
    });
}

export function* connectWebSocket() {
    const sessionData = yield select(selectorsConnector.getSessionData);
    const socketPath = yield select(selectorsConnector.getSockPath);
    yield call(initStompClient, socketPath);
    channel = yield call(connectStompClient, sessionData.su, sessionData.sp, sessionData.sid);

    while (stompClient) {
        const eventAction = yield take(channel);
        if (!eventAction) {
            closeWebSocket();
            return false;
        }
        yield put(eventAction);
    }
}

export function* sendRequest(message) {
    if (!stompClient || !message) {
        return false;
    }

    yield apply(stompClient, stompClient.send, ['/exchange/CMD/', {}, message]);
}
