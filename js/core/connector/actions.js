import constants from '../constants/constants';

export const connect = () => ({ type: constants.CONNECT });
export const connectWs = () => ({ type: constants.CONNECT_WS });
export const connectWsSuccess = payload => ({ type: constants.CONNECT_WS_SUCCESS, payload });
export const connectWsError = payload => ({ type: constants.CONNECT_WS_ERROR, payload });
export const connectWsMessage = payload => ({ type: constants.CONNECT_WS_MESSAGE, payload });
export const reconnectRequest = () => ({ type: constants.RECONNECT_REQUEST });
export const initConnection = () => ({ type: constants.INIT_CONNECTION });
export const keepAliveRequest = () => ({ type: constants.KEEP_ALIVE_REQUEST });
export const logout = payload => ({ type: constants.LOGOUT, payload });
export const setSessionData = payload => ({ type: constants.SET_SESSION_DATA_STORE, payload });
export const setConnectionStateStore = payload => ({ type: constants.SET_CONNECTION_STATE_STORE, payload });
export const setItemSidsLS = payload => ({ type: constants.LS_SET_ITEM_SIDS, payload });
export const setItemSidsStore = payload => ({ type: constants.SET_SIDS_STORE, payload });
export const crmSSORequest = payload => ({ type: constants.CRM_SSO_REQUEST, payload });
export const setItemSessionTid = payload => ({ type: constants.LS_SET_ITEM_SESSION_TID, payload });
export const sendRequest = payload => ({ type: constants.SEND_REQUEST, payload });
export const error = payload => ({ type: constants.ERROR, payload });
