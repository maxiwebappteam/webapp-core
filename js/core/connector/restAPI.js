import { take, put, call, apply } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

let xmlHttp = null;
let keepAliveXmlHttp = null;

export function request(action, params) {
    return eventChannel(emitter => {
        try {
            const xmlHttpRequest = action === 'keep_alive_request' ? keepAliveXmlHttp : xmlHttp;

            xmlHttpRequest.onreadystatechange = () => {
                (xmlHttpRequest.readyState === 4) && onResponse(emitter, xmlHttpRequest, action);
            };

            xmlHttpRequest.send(params);
        } catch (error) {
            console.error(error);
            onError(emitter);
        }

        return () => abortXMLHttp();
    });
}

export function getXMLHttp() {
    return new XMLHttpRequest();
}

export function abortXMLHttp() {
    if (xmlHttp) {
        xmlHttp.abort();
        xmlHttp = null;
    }

    if (keepAliveXmlHttp) {
        keepAliveXmlHttp.abort();
        keepAliveXmlHttp = null;
    }
}

export function* initXMLHttp(type, address, timeout) {
    xmlHttp = yield call(createXMLHttp, type, address, timeout);
    keepAliveXmlHttp = yield call(createXMLHttp, type, address, timeout);
}

export function* createXMLHttp(type, address, timeout) {
    const xmlHttpRequest = yield call(getXMLHttp);
    yield apply(xmlHttpRequest, xmlHttpRequest.open, [type, address]);
    yield apply(xmlHttpRequest, xmlHttpRequest.setRequestHeader, ['Content-type', 'application/x-www-form-urlencoded']);
    xmlHttpRequest.timeout = timeout;
    return xmlHttpRequest;
}

export function onResponse(emitter, response, action) {
    try {
        if (!action) {
            return false;
        }

        const payload = {
            readyState: response.readyState,
            status: response.status,
            responseText: response.responseText,
        };

        if (response.status === 200) {
            emitter({
                type: `${action}_success`,
                payload: payload,
            });
        } else {
            emitter({
                type: `${action}_failure`,
                payload: payload,
            });
        }
    } catch (error) {
        console.error(`onResponse ${error}`);
    }
}

export function onError(emitter) {
    try {
        emitter({ isError: true });
    } catch (error) {
        console.error(`onError ${error}`);
    }
}

export function* requestGET(action, address, timeout) {
    yield call(initXMLHttp, `GET`, address, timeout);
    const channel = yield call(request, action, null);
    const response = yield take(channel);

    if (response.isError) {
        channel.close();
        return false;
    }

    yield put(response);
    channel.close();
}

export function* requestPOST(action, address, params, timeout) {
    yield call(initXMLHttp, `POST`, address, timeout);
    const channel = yield call(request, action, params);
    const response = yield take(channel);

    if (response.isError) {
        channel.close();
        return false;
    }

    yield put(response);
    channel.close();
}
