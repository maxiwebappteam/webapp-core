import { convertMessageToBytes, decodeMessage } from '../../utils/decodeMessage';
import { decode } from 'msgpack-lite';
import { call, put } from 'redux-saga/effects';
import * as actions from '../actions';

let rootCommandKeeper = null;

export function combine(requestSenders) {
    return initCommandKeeper(requestSenders);
}

export function* parseResult(action) {
    if (!action) {
        return false;
    }

    const jsonobject = decodeMessage(decode(convertMessageToBytes(action.payload.body)));
    let restype = jsonobject.type;
    let eventtype = jsonobject.type === 'event' ? jsonobject.name : jsonobject.cmd;
    let rescontent = jsonobject.content;

    if (restype === 'response' && jsonobject.code !== 1) {
        yield put(actions.error(jsonobject));
        return false;
    }

    if (rootCommandKeeper[restype] && rootCommandKeeper[restype][eventtype]) {
        yield call(rootCommandKeeper[restype][eventtype], rescontent);
    } else {
        console.warn(`Warning. Server response wasn't parsed while parseResult() [restype=${restype}, eventtype=${eventtype}]`);
        return false;
    }
}

export const initParsers = (commandKeeper) => {
    rootCommandKeeper = commandKeeper;
};

function initCommandKeeper(parsers) {
    let commandKeeper = {};

    for (let key in parsers) {
        commandKeeper = {
            update: {
                ...commandKeeper.update,
                ...parsers[key].update,
            },
            response: {
                ...commandKeeper.response,
                ...parsers[key].response,
            },
            event: {
                ...commandKeeper.event,
                ...parsers[key].event,
            },
        };
    }

    return commandKeeper;
}
