import { put, select } from 'redux-saga/effects';
import constants from '../../constants/constants';
import * as actionsConnector from '../actions';
import * as selectorsConnector from '../selectors';

function* logout(rescontent) {
    yield put(actionsConnector.logout(rescontent));
}

function* getTradingStatus(rescontent) {
    if (!rescontent || !rescontent.tradingDisabled) {
        return false;
    }

    const lang = yield select(selectorsConnector.getLanguage);
    yield put(actionsConnector.logout(`err=trading_disabled&lang=${lang}`));
}

function* tradingServerConnectionLost() {
    yield put(actionsConnector.setConnectionStateStore(false));
    // connector.toggleReconnectSpinner(true);
}

function* setCrmSingleSignOn(rescontent) {
    if (!rescontent || !rescontent.token || !rescontent.currentAccountCrmId) {
        console.warn(`Warning! Request wasn't sent while SSO. Parameters isn't exist`);
        return false;
    }

    yield put(actionsConnector.crmSSORequest({ token: rescontent.token, currentAccountCrmId: rescontent.currentAccountCrmId }));
}

export default {
    update: {
        [constants.GET_TRADING_STATUS]: getTradingStatus,
        [constants.CRM_SINGLE_SIGN_ON]: setCrmSingleSignOn,
    },
    response: {
        [constants.LOGOUT]: logout,
        [constants.GET_TRADING_STATUS]: getTradingStatus,
    },
    event: {
        [constants.TRADING_SERVER_CONNECTION_LOST]: tradingServerConnectionLost,
    },
};
