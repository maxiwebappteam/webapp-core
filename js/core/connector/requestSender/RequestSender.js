let sessionId = null;
let customMarker = null;
let rootCommandKeeper = null;

function initCommandKeeper(requestSenders) {
    let commandKeeper = {};
    for (let key in requestSenders) {
        commandKeeper = {
            ...commandKeeper,
            ...requestSenders[key],
        };
    }

    return commandKeeper;
}

export function combineRequest(requestSenders) {
    return initCommandKeeper(requestSenders);
}

export const getRequestString = (command, params) => {
    if (!customMarker || !sessionId || !rootCommandKeeper || !rootCommandKeeper[command]) {
        console.error(`no session params sessionId = ${sessionId} customMarker = ${customMarker}`);
        return false;
    }

    return rootCommandKeeper[command]({ sessionId, customMarker }, params);
};

export const initRequestSender = (commandKeeper) => {
    rootCommandKeeper = commandKeeper;
};

export const initRequestSenderSessionData = (id, cusMarker) => {
    sessionId = id;
    customMarker = cusMarker;
};
