import { getCookie } from '../../utils/getCookie';
import constants from '../../constants/constants';

const getSessionData = (sessionId, action, params) => ({
    sid: sessionId,
    umid: getCookie('um_session'),
    cmd: action,
    params: params,
});

const logout = (context = {}, params = []) => JSON.stringify(
    getSessionData(context.sessionId, constants.LOGOUT, params)
);

const getTradingStatus = (context = {}, params = []) => JSON.stringify(
    getSessionData(context.sessionId, constants.GET_TRADING_STATUS, params)
);

export default {
    [constants.LOGOUT]: logout,
    [constants.GET_TRADING_STATUS]: getTradingStatus,
};

