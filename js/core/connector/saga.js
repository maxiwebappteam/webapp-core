import { takeEvery, take, put, call, select } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import constants from '../constants/constants';
import * as connectorActions from './actions';
import * as selectorsConnector from './selectors';
import { requestGET, requestPOST } from './restAPI';
import { connectWebSocket, closeWebSocket, sendRequest } from './webSocket';
import { initRequestSenderSessionData, getRequestString } from './requestSender/RequestSender';
import { parseResult } from './parser/Parser';
import { getCurrentTime } from '../utils/getCurrentTime';

let isDuplicate = false;
let keepAliveInterval;
let channelKeepAlive;
let reconnectionState = false;
let sids;

export default function* watchConnector() {
    yield takeEvery(constants.INIT_CONNECTION, init);
    yield takeEvery(constants.CONNECT, connect);
    yield takeEvery(constants.CONNECT_WS, connectWebSocket);
    yield takeEvery(constants.CONNECT_WS_SUCCESS, connectWebSocketSuccess);
    yield takeEvery(constants.CONNECT_WS_MESSAGE, parseResult);
    yield takeEvery(constants.CONNECT_WS_ERROR, connectWebSocketError);
    yield takeEvery(constants.LOGOUT, logout);
    yield takeEvery(constants.KEEP_ALIVE_REQUEST, keepAliveRequest);
    yield takeEvery(constants.KEEP_ALIVE_REQUEST_SUCCESS, keepAliveRequestSuccess);
    yield takeEvery(constants.KEEP_ALIVE_REQUEST_FAILURE, keepAliveRequestFailure);
    yield takeEvery(constants.RECONNECT_REQUEST, reconnectRequest);
    yield takeEvery(constants.RECONNECT_REQUEST_SUCCESS, reconnectRequestSuccess);
    yield takeEvery(constants.RECONNECT_REQUEST_FAILURE, reconnectRequestFailure);
    yield takeEvery(constants.CHANGE_LANGUAGE, changeLanguage);
    yield takeEvery(constants.CRM_SSO_REQUEST, crmSSORequest);
    yield takeEvery(constants.CRM_SSO_REQUEST_SUCCESS, crmSSORequestSuccess);
    yield takeEvery(constants.CRM_SSO_REQUEST_FAILURE, crmSSORequestFailure);
    yield takeEvery(constants.WINDOW_UNLOAD, unloadLocalStorageManagerSids);
    yield takeEvery(constants.SEND_REQUEST, onSendRequest);
}

export function* init() {
    const sessionData = yield select(selectorsConnector.getSessionData);
    const isLocalStorageSupport = yield select(selectorsConnector.isLocalStorageSupport);
    const keepAliveParams = yield select(selectorsConnector.getKeepAliveParams);
    if (!sessionData.sid || !sessionData.su || !sessionData.sp) {
        yield put(connectorActions.logout(`code=NOSESSION&lang=${sessionData.lang}`));
    } else {
        if (!isLocalStorageSupport) {
            yield put(connectorActions.logout(`code=LSNOSUPPORT&lang=${sessionData.lang}`));
        }

        sids = sessionData.sids ? { ...sessionData.sids } : {};

        if (sids[sessionData.sid]) {
            isDuplicate = true;
            yield put(connectorActions.logout(`code=DUPLICATE&lang=${sessionData.lang}`));
        } else {
            sids[sessionData.sid] = yield call(getCurrentTime);

            yield put(connectorActions.setItemSidsLS(sids));
            yield put(connectorActions.setItemSidsStore(sids));
        }
    }

    yield put(connectorActions.connect());

    if (keepAliveParams.isKeepAlive) {
        channelKeepAlive && channelKeepAlive.close();
        yield call(connKeepAlive);
    }
}

export function* unloadLocalStorageManagerSids() {
    const isLocalStorageSupport = yield select(selectorsConnector.isLocalStorageSupport);
    if (!isDuplicate && isLocalStorageSupport) {
        const sessionData = yield select(selectorsConnector.getSessionData);
        const sessionIds = sessionData.sids ? { ...sessionData.sids } : {};
        const removedSids = yield call(getRemovedSid, sessionIds, sessionData.sid);
        yield put(connectorActions.setItemSidsLS(removedSids));
    }
}

export function* connect() {
    if (reconnectionState) {
        reconnectionState = false;
    } else {
        const connectionStatus = yield call(getConnectionStatusLocalStorage);
        if (!connectionStatus) {
            yield put(connectorActions.reconnectRequest());
            return false;
        }
    }

    const sessionId = yield select(selectorsConnector.getSessionId);
    const customMarker = yield select(selectorsConnector.getCustomMarker);
    yield call(initRequestSenderSessionData, sessionId, customMarker);
    yield put(connectorActions.connectWs());
}

export function* reconnectRequest(action) {
    reconnectionState = true;
    yield call(closeWebSocket);
    const reconnectParams = yield select(selectorsConnector.getReconnectParams);
    const sessionData = yield select(selectorsConnector.getSessionData);
    yield call(requestPOST, action.type, reconnectParams.loginService, `su=${sessionData.su}&sp=${sessionData.sp}`, reconnectParams.keepAliveTimeOut);
}

export function* reconnectRequestSuccess(action) {
    const sessionData = { ...yield select(selectorsConnector.getSessionData) };
    const res = JSON.parse(action.payload.responseText);

    if (res.sid) {
        let newSession = {
            sid: res.sid,
            uid: sessionData.uid,
            lang: sessionData.lang,
            th: sessionData.th,
            su: res.stompUser,
            sp: res.stompPassword,
            isConnectAlive: true,
            tid: sessionData.tid,
        };

        yield put(connectorActions.setItemSessionTid({
            itemName: `_session_${sessionData.tid}`,
            itemObject: newSession,
        }));
        yield put(connectorActions.setSessionData(newSession));
        yield put(connectorActions.connect());
    } else {
        yield put(connectorActions.logout(`REGENERATE_ERROR=${res.code}`));
    }
}

export function* reconnectRequestFailure() {
    yield put(connectorActions.setConnectionStateStore(false));
    yield call(setConnectionStatusLocalStorage, false);
    yield put(connectorActions.reconnectRequest());
}

export function* getConnectionStatusLocalStorage() {
    const isConnectionAlive = yield select(selectorsConnector.isConnectionAlive);
    return isConnectionAlive;
}

export function* setConnectionStatusLocalStorage(status) {
    const sessionData = { ...yield select(selectorsConnector.getSessionData) };
    sessionData.isConnectAlive = status;
    yield put(connectorActions.setItemSessionTid({ itemName: `_session_${sessionData.tid}`, itemObject: sessionData }));
    yield put(connectorActions.setSessionData(sessionData));
}

export function* connKeepAlive() {
    const keepAliveParams = yield select(selectorsConnector.getKeepAliveParams);

    channelKeepAlive = yield call(intervalEventChannel, keepAliveParams.keepAliveTimeOut);
    while (keepAliveInterval) {
        const channelAction = yield take(channelKeepAlive);
        yield put(channelAction);
    }
}

export function intervalEventChannel(keepAliveTimeOut) {
    return eventChannel(emitter => {
        keepAliveInterval = setInterval(() => {
            emitter(connectorActions.keepAliveRequest());
        }, keepAliveTimeOut);

        return () => {
            clearInterval(keepAliveInterval);
            keepAliveInterval = null;
        };
    });
}

export function* keepAliveRequest(action) {
    const keepAliveParams = yield select(selectorsConnector.getKeepAliveParams);
    const sessionData = yield select(selectorsConnector.getSessionData);
    const time = yield call(getCurrentTime);
    yield call(requestGET, action.type, `${keepAliveParams.keepAliveService}?sid=${sessionData.sid}&t=${time}`, keepAliveParams.keepAliveTimeOut);
}

export function* keepAliveRequestSuccess() {
    // console.info(action);
}

export function* keepAliveRequestFailure(action) {
    const sessionData = yield select(selectorsConnector.getSessionData);
    const status = action.payload.status;

    if (status === 502 || status === 400 || status === 404) {
        yield put(connectorActions.logout(`code=${status}&lang=${sessionData.lang}`));
    }

    if (!reconnectionState) {
        yield put(connectorActions.setConnectionStateStore(false));
        yield put(connectorActions.reconnectRequest());
    }
}

export function* changeLanguage(action) {
    const sessionData = { ...yield select(selectorsConnector.getSessionData) };

    if (!sessionData.isConnectAlive || !action) {
        return false;
    }

    sessionData.lang = action.payload;

    yield put(connectorActions.setItemSessionTid({ itemName: `_session_${sessionData.tid}`, itemObject: sessionData }));
    yield put(connectorActions.setSessionData(sessionData));
}

export function* crmSSORequest(action) {
    const connectionsParams = yield select(selectorsConnector.getKeepAliveParams);
    const getCRMApiUrl = yield select(selectorsConnector.getCRMApiUrl);
    yield call(requestPOST, action.type, `${getCRMApiUrl}/account/logonWithToken`, `token=${action.payload.token}`, connectionsParams.keepAliveTimeOut);
}

export function* crmSSORequestSuccess() {
    // console.info(action);
}

export function crmSSORequestFailure(action) {
    console.warn(`Warning! Request wasn't sent while SSO status=${action.payload.status}`);
}

export function* connectWebSocketSuccess() {
    yield call(sendRequest, getRequestString(constants.GET_TRADING_STATUS));
}

export function* connectWebSocketError(action) {
    if (reconnectionState) {
        return false;
    }

    yield call(closeWebSocket);
    const error = action.payload;
    const lang = yield select(selectorsConnector.getLanguage);

    if (error && error.headers) {
        switch (error.headers.message) {
            case 'not_found':
                yield put(connectorActions.reconnectRequest());
                break;
            case 'access_refused':
            case 'Bad CONNECT':
                yield put(connectorActions.logout(`err=session_expired&lang=${lang}`));
                break;
            case 'Server cancelled subscription':
                yield put(connectorActions.logout(`err=session_canceled&lang=${lang}`));
                break;
            default:
                yield put(connectorActions.connect());
        }

        console.error(error.headers.message, error.body);
    } else {
        yield put(connectorActions.reconnectRequest());
    }
}

export function getRemovedSid(sessionIds, currentId) {
    const time = new Date().getTime() - 86400000;
    let newSids = {};

    for (let id in sessionIds) {
        if (sessionIds.hasOwnProperty(id)) {
            if (id === currentId) {
                continue;
            }
            if (sessionIds[id] > time) {
                newSids[id] = sessionIds[id];
            }
        }
    }
    return newSids;
}

export function* logout(action) {
    const sessionData = yield select(selectorsConnector.getSessionData);
    const actionType = yield call(getRequestString, action.type);
    yield call(sendRequest, actionType);
    yield call(closeWebSocket);
    channelKeepAlive && channelKeepAlive.close();

    const closedSession = {
        lang: sessionData.lang,
        th: sessionData.th,
    };
    const removedSids = yield call(getRemovedSid, sids, sessionData.sid);
    yield put(connectorActions.setItemSidsLS(removedSids));
    yield put(connectorActions.setItemSessionTid({ itemName: `_session_${sessionData.tid}`, itemObject: closedSession }));
    const logoutParams = action.payload ? `login.html?${action.payload}` : `login.html?v=1&op=logout&lang=${sessionData.lang}`;
    yield call(assignLocation, logoutParams);
}

export function assignLocation(params) {
    params && window.location.assign(params);
}

export function* onSendRequest(action) {
    const message = yield call(getRequestString, action.payload.command, action.payload.params);
    yield call(sendRequest, message);
}
