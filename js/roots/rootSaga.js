import { all, call } from 'redux-saga/effects';
import { coreSaga } from '../core/coreAPI';

const sagasList = [
    ...coreSaga,
];

export default function* watchRootSaga() {
    yield all(sagasList.map(saga => call(saga)));
}
