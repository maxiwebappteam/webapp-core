import { combineReducers } from 'redux';
import { coreReducer } from '../core/coreAPI';

const rootReducer = combineReducers({
    ...coreReducer,
});

export default rootReducer;
